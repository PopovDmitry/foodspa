import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";
import { LoadingComponent, ErrorGooglePLayComponent } from './components';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/loading'
    },
    {
        path: "loading",
        pathMatch: "full",
        component: LoadingComponent
    },
    {
        path: "activation",
        loadChildren: () => require("./modules/activation/activation.module")["ActivationModule"]
    },
    {
        path: "index",
        loadChildren: () => require("./modules/index/index.module")["IndexModule"]
    },
    {
        path: "basket",
        loadChildren: () => require("./modules/basket/basket.module")["BasketModule"]
    },
    {
        path: "errorGooglePLay",
        component: ErrorGooglePLayComponent

    }

    // { path: "", redirectTo: "/main", pathMatch: "full" },
    // { path: "main", component: MainPageComponent },
    // { path: "getPhone", component: GetPhoneComponent },
    // { path: "getCode", component: GetCodeComponent },
    // { path: "item/:id", component: ItemDetailComponent },
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
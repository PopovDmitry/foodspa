"use strict";
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var router_1 = require("nativescript-angular/router");
var services_1 = require("../../services");
var models_1 = require("../../models");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var components_1 = require("../../components");
var BasketTwoComponent = (function () {
    function BasketTwoComponent(sharedService, page, router, _modalService, vcRef) {
        this.sharedService = sharedService;
        this.page = page;
        this.router = router;
        this._modalService = _modalService;
        this.vcRef = vcRef;
        this.listViewHeight = 0;
        this.date = '';
        this.time = '';
        this.sdvigHours = 3;
        this.starDevileringInHours = 9;
    }
    BasketTwoComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
        this.page.backgroundSpanUnderStatusBar = true;
        this.listViewHeight = this.sharedService.user.screen_height - 30;
        this.class = (this.sharedService.isIOS) ? 'ios' : 'android';
        this.dateObj = new Date();
        this.createDate(this.dateObj, true);
        if (this.sharedService.contacts.length > 0) {
            this.sharedService.basket.contact_id = this.sharedService.contacts[0].id;
        }
    };
    BasketTwoComponent.prototype.showDataPiker = function () {
        var _this = this;
        var options = {
            viewContainerRef: this.vcRef,
            context: {
                date: this.dateObj
            },
            fullscreen: false
        };
        this._modalService.showModal(components_1.DateTimeComponent, options)
            .then(function (v) {
            if (v.result != undefined) {
                _this.dateObj = v.result;
                _this.createDate(_this.dateObj);
            }
        });
    };
    BasketTwoComponent.prototype.createDate = function (d, isNow) {
        if (isNow === void 0) { isNow = false; }
        var months = [
            'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'
        ];
        var m = d.getMonth();
        var day = d.getDate();
        var hour = d.getHours();
        var minuts = 0;
        if (isNow) {
            var z = new Date();
            var z2 = new Date(z.setHours(z.getHours() + this.sdvigHours));
            if (z2.getHours() >= 22 && day == z2.getDate()) {
                var newDate = new Date(z2.getFullYear(), z2.getMonth(), z2.getDate(), this.starDevileringInHours, 0, 0);
                newDate = new Date(newDate.setDate(z.getDate() + 1));
            }
            else if (day != z2.getDate()) {
                var newDate = new Date(z2.getFullYear(), z2.getMonth(), z2.getDate(), this.starDevileringInHours, 0, 0);
            }
            else {
                var newDate = z2;
            }
            if (newDate != undefined) {
                m = newDate.getMonth();
                day = newDate.getDate();
                hour = newDate.getHours();
                this.dateObj = newDate;
            }
        }
        var hourFormat = (hour < 10) ? '0' + hour.toString() : hour;
        var minFormat = (minuts < 10) ? '0' + minuts.toString() : minuts;
        var dayFormat = (day < 10) ? '0' + day.toString() : day;
        this.date = dayFormat + ' ' + months[m];
        this.time = hourFormat + ':' + minFormat + ' - ' + hour + ':' + 59;
        this.sharedService.basket.date_time = new models_1.DateTime(this.date, this.time);
    };
    BasketTwoComponent.prototype.nextStep = function () {
        this.sharedService.basket.comment = this.comment.nativeElement.text;
        this.sharedService.saveBasket().then(function (s) { }, function (err) { });
        this.router.navigate(["basket/three"], {});
    };
    BasketTwoComponent.prototype.addAdress = function () {
        var _this = this;
        var options = {
            viewContainerRef: this.vcRef,
            context: { type: 'add' },
            fullscreen: false
        };
        this._modalService.showModal(components_1.AddressComponent, options)
            .then(function (v) {
            if (_this.sharedService.addresses.length == 1) {
                _this.sharedService.basket.address_id = _this.sharedService.addresses[0].id;
            }
        }, function (err) { });
    };
    BasketTwoComponent.prototype.editAdress = function (data, index) {
        var options = {
            viewContainerRef: this.vcRef,
            context: { type: 'edit', data: data, index: index },
            fullscreen: false
        };
        this._modalService.showModal(components_1.AddressComponent, options)
            .then(function (v) {
            if (v.result) {
            }
        }, function (err) { });
    };
    BasketTwoComponent.prototype.createContact = function () {
        var _this = this;
        var options = {
            viewContainerRef: this.vcRef,
            context: { type: 'add' },
            fullscreen: false
        };
        this._modalService.showModal(components_1.ContactComponent, options)
            .then(function (v) {
            if (_this.sharedService.contacts.length == 1) {
                _this.sharedService.basket.contact_id = _this.sharedService.contacts[0].id;
            }
        }, function (err) {
        });
    };
    BasketTwoComponent.prototype.editContact = function (data, index) {
        var options = {
            viewContainerRef: this.vcRef,
            context: { type: 'edit', data: data, index: index },
            fullscreen: false
        };
        this._modalService.showModal(components_1.ContactComponent, options)
            .then(function (v) {
        }, function (err) { });
    };
    BasketTwoComponent.prototype.doneTap = function (args) {
        var myTextField = args.object;
        myTextField.dismissSoftInput();
    };
    return BasketTwoComponent;
}());
__decorate([
    core_1.ViewChild("comment"),
    __metadata("design:type", core_1.ElementRef)
], BasketTwoComponent.prototype, "comment", void 0);
BasketTwoComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'basket-two',
        templateUrl: 'basket-two.component.html',
        styleUrls: ['basket-two.css']
    }),
    __metadata("design:paramtypes", [services_1.SharedService,
        page_1.Page,
        router_1.RouterExtensions,
        modal_dialog_1.ModalDialogService,
        core_1.ViewContainerRef])
], BasketTwoComponent);
exports.BasketTwoComponent = BasketTwoComponent;

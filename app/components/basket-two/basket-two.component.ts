import { Component, OnInit, ViewContainerRef, ElementRef, ViewChild } from '@angular/core';
import { Page } from "ui/page";
import { RouterExtensions } from "nativescript-angular/router";
import { SharedService } from '../../services'
import { Product, ProductsBasket, Address, Contact, DateTime } from '../../models';
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { AddressComponent, ContactComponent, DateTimeComponent } from '../../components'
import datePickerModule = require("ui/date-picker");

@Component({
    moduleId: module.id,
    selector: 'basket-two',
    templateUrl: 'basket-two.component.html',
    styleUrls: ['basket-two.css']
})
export class BasketTwoComponent implements OnInit {

    @ViewChild("comment") comment: ElementRef;
    public listViewHeight: number = 0;
    private date: string = ''
    private time: string = ''
    private dateObj;
    private class: string;
    private sdvigHours: number = 3; // сдвиг времени доставки
    private starDevileringInHours: number = 9; // время начало доставки


    constructor(
        public sharedService: SharedService,
        private page: Page,
        private router: RouterExtensions, 
        private _modalService: ModalDialogService,
        private vcRef: ViewContainerRef
    ) { }

    ngOnInit() {
        this.page.actionBarHidden = true;  
        this.page.backgroundSpanUnderStatusBar = true;
        this.listViewHeight = this.sharedService.user.screen_height - 30;
        this.class = (this.sharedService.isIOS) ? 'ios' : 'android';
        

        // формируем текущию дату
        this.dateObj = new Date();
        this.createDate(this.dateObj, true);

        
        // если есть хотябы один контакт
        if(this.sharedService.contacts.length > 0){
            // выбираем самый первый 
            this.sharedService.basket.contact_id = this.sharedService.contacts[0].id;
        }
     }

     showDataPiker(){
         let options: ModalDialogOptions = {
            viewContainerRef: this.vcRef,
            context: {
                date: this.dateObj
            },
            fullscreen: false
        };
        this._modalService.showModal(DateTimeComponent, options)
        .then((v) => {
            if(v.result != undefined){
                this.dateObj = v.result
                this.createDate(this.dateObj)
            }
        })
     }

     createDate(d, isNow: boolean = false){
         let months = [
             'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'
         ];

        var m = d.getMonth();
        var day = d.getDate();
        var hour = d.getHours();
        // let minuts = d.getMinutes();
        let minuts: number = 0;

        // если сегодня
        if(isNow){
            
            var z = new Date();
            // z.addHours(this.sdvigHours);

            var z2 = new Date(z.setHours(z.getHours() + this.sdvigHours));
            
            if(z2.getHours() >= 22 && day == z2.getDate()){
                // после 23 ставим дату следующий день 9 утра
                var newDate = new Date(
                    z2.getFullYear(),
                    z2.getMonth(),
                    z2.getDate(),
                    this.starDevileringInHours,
                    0,
                    0
                )

                newDate = new Date(newDate.setDate(z.getDate() + 1))
            } else if(day != z2.getDate()){
                var newDate = new Date(
                    z2.getFullYear(),
                    z2.getMonth(),
                    z2.getDate(),
                    this.starDevileringInHours,
                    0,
                    0
                )
            }else{
                var newDate = z2;
            }

            if(newDate != undefined ){
                m = newDate.getMonth();
                day = newDate.getDate();
                hour = newDate.getHours();
                this.dateObj = newDate;
            }
        }

        let hourFormat = (hour < 10) ? '0' + hour.toString() : hour;
        let minFormat = (minuts < 10) ? '0' + minuts.toString() : minuts;
        let dayFormat = (day < 10) ? '0' + day.toString() : day;
        

        this.date = dayFormat + ' ' + months[m];
        this.time = hourFormat + ':' + minFormat + ' - ' + hour + ':' + 59;

        // console.log('this.date', this.date)
        // console.log('this.time', this.time)

        this.sharedService.basket.date_time = new DateTime(this.date, this.time)

        // this.sharedService.basket.date_time.date = this.date;
        // this.sharedService.basket.date_time.time = this.time;

     }

     nextStep(){
        this.sharedService.basket.comment = this.comment.nativeElement.text;
        this.sharedService.saveBasket().then(s => {}, err => {})
        // this.sharedService.saveBasket().then(success => {
            this.router.navigate(["basket/three"], {
            });
        // });
     }

     // добалвение адреса
     addAdress(){
        //  console.log('-----------------------------0')
        let options: ModalDialogOptions = {
            viewContainerRef: this.vcRef,
            context: {type: 'add'},
            fullscreen: false
        };
        this._modalService.showModal(AddressComponent, options)
        .then((v) => {
            // console.log('-----------------------------4')

            if(this.sharedService.addresses.length == 1){ // если первый созданный контакт то ставим его по умолчанию
                this.sharedService.basket.address_id = this.sharedService.addresses[0].id;
            }
            // if(v.result){
            //     // удачное сохранение    
            // }
        }, err => {})
     }

     // редактирование адреса
     editAdress(data: Address, index: number){
        let options: ModalDialogOptions = {
            viewContainerRef: this.vcRef,
            context: {type: 'edit', data: data, index: index},
            fullscreen: false
        };
        this._modalService.showModal(AddressComponent, options)
        .then((v) => {
            if(v.result){

            }
        }, err => {})
     }

     // создание контактта
     createContact(){
         let options: ModalDialogOptions = {
            viewContainerRef: this.vcRef,
            context: {type: 'add'},
            fullscreen: false
        };
        this._modalService.showModal(ContactComponent, options)
        .then((v) => {
            if(this.sharedService.contacts.length == 1){ // если первый созданный контакт то ставим его по умолчанию
                this.sharedService.basket.contact_id = this.sharedService.contacts[0].id;
            }
        }, err => {

        })
     }

     // редактирование контактта
     editContact(data: Contact, index: number){
        let options: ModalDialogOptions = {
            viewContainerRef: this.vcRef,
            context: {type: 'edit', data: data, index: index},
            fullscreen: false
        };
        this._modalService.showModal(ContactComponent, options)
        .then((v) => {
            
        }, err => {})
     }

     doneTap(args) {
        var myTextField = args.object;
        myTextField.dismissSoftInput();
    }
}
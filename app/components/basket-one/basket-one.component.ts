import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Page } from "ui/page";
import { RouterExtensions } from "nativescript-angular/router";
import { SharedService } from '../../services'
import { Product, ProductsBasket } from '../../models'
import dialogs = require("ui/dialogs");
import { find } from 'lodash';
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { ModalCountComponent } from '../../components'


@Component({
    moduleId: module.id,
    selector: 'basket-one',
    templateUrl: 'basket-one.component.html',
    styleUrls: ['basket-one.css']
})
export class BasketOneComponent implements OnInit {
    public products: any[] = [];
    public allSum: number = 0;
    public listViewHeight: number = 0;


    private class = '';

    constructor(  
        public sharedService: SharedService,
        private page: Page,
        private router: RouterExtensions, 
        private _modalService: ModalDialogService,
        private vcRef: ViewContainerRef
    ) {
        this.page.actionBarHidden = true;  
        this.listViewHeight = this.sharedService.user.screen_height - 30;
        this.page.backgroundSpanUnderStatusBar = true;
        
     }

    ngOnInit() { 
        this.basketInfoToProducts(); 
        this.class = (this.sharedService.isIOS) ? 'ios' : 'android'

        // console.log('start')
    }

    // конвертируем данные о продуктах из корзины в массив
    basketInfoToProducts():any[] {
        // this.products = [];
        let products: any = [];
        this.allSum = 0;

        this.sharedService.basket.products.forEach(v => {
            
            let pr = this.getProductById(v.product_id);

            if(pr != null){
                products.push({
                    info: pr,
                    count: v.product_count,
                    summa: v.product_count * pr.price
                })
                this.allSum += v.product_count * pr.price;
            } 
        })
        // console.dump(this.products)
        this.products = products;
        return this.products;
    } 

    // получаем продукт по его ID
    getProductById(id: number): any{
        return this.sharedService.data.reduce((result, value, key) => {
            let res = find(value['products'], v => v.pid == id)
            if(res != null){
                result = res;
            }
            return result;
        },{})
    }

    // удаление из корзины 
    deleteFromBasket(id: number, name: string): void{
        // console.log(id, name);
        dialogs.confirm("Удалить " + name).then(result => {
            console.log('DIALOFGS', result)
            if(result){
                this.sharedService.basket.products.forEach( (v, k) => {
                    if(v.product_id == id){
                        this.sharedService.basket.products.splice(k, 1);
                    }
                })    
                this.basketInfoToProducts()
                this.sharedService.sendNext(this.sharedService.eventTypes.actions.addbasket, true);
                this.sharedService.saveBasket();
            }
            
        });
    }

    // изменить колличество
    editCol(item: ProductsBasket, index){
        let options: ModalDialogOptions = {
            viewContainerRef: this.vcRef,
            context: this.sharedService.basket.products[index].product_count,
            fullscreen: false
        };
        this._modalService.showModal(ModalCountComponent, options)
        .then((count) => {
            console.log("+++++++++++++++++++++++++++COUNT", count)
            if(count == 0){
                // ничего не добавлять
            }else{
                // добавить ное колличестов
                this.sharedService.basket.products[index].product_count = count;
                this.sharedService.sendNext(this.sharedService.eventTypes.actions.addbasket, true);
                this.basketInfoToProducts();
                this.sharedService.saveBasket().then(s => {}, err => {});
            }
        });
    }

    // Сохранение и переход на следующи шаг
    nextStep(){
        this.sharedService.saveBasket().then(success => {
            setTimeout(() => {
                this.router.navigate(["basket/two"]);
            }, 200)
            
        }, err => {
            this.router.navigate(["basket/two"]);
        });
    }

    goMain(){
        this.sharedService.saveBasket().then(success => {
            this.router.navigate(["index"], {});
        }, err => {
            
        });
    }
}
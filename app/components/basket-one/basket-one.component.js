"use strict";
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var router_1 = require("nativescript-angular/router");
var services_1 = require("../../services");
var dialogs = require("ui/dialogs");
var lodash_1 = require("lodash");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var components_1 = require("../../components");
var BasketOneComponent = (function () {
    function BasketOneComponent(sharedService, page, router, _modalService, vcRef) {
        this.sharedService = sharedService;
        this.page = page;
        this.router = router;
        this._modalService = _modalService;
        this.vcRef = vcRef;
        this.products = [];
        this.allSum = 0;
        this.listViewHeight = 0;
        this.class = '';
        this.page.actionBarHidden = true;
        this.listViewHeight = this.sharedService.user.screen_height - 30;
        this.page.backgroundSpanUnderStatusBar = true;
    }
    BasketOneComponent.prototype.ngOnInit = function () {
        this.basketInfoToProducts();
        this.class = (this.sharedService.isIOS) ? 'ios' : 'android';
    };
    BasketOneComponent.prototype.basketInfoToProducts = function () {
        var _this = this;
        var products = [];
        this.allSum = 0;
        this.sharedService.basket.products.forEach(function (v) {
            var pr = _this.getProductById(v.product_id);
            if (pr != null) {
                products.push({
                    info: pr,
                    count: v.product_count,
                    summa: v.product_count * pr.price
                });
                _this.allSum += v.product_count * pr.price;
            }
        });
        this.products = products;
        return this.products;
    };
    BasketOneComponent.prototype.getProductById = function (id) {
        return this.sharedService.data.reduce(function (result, value, key) {
            var res = lodash_1.find(value['products'], function (v) { return v.pid == id; });
            if (res != null) {
                result = res;
            }
            return result;
        }, {});
    };
    BasketOneComponent.prototype.deleteFromBasket = function (id, name) {
        var _this = this;
        dialogs.confirm("Удалить " + name).then(function (result) {
            console.log('DIALOFGS', result);
            if (result) {
                _this.sharedService.basket.products.forEach(function (v, k) {
                    if (v.product_id == id) {
                        _this.sharedService.basket.products.splice(k, 1);
                    }
                });
                _this.basketInfoToProducts();
                _this.sharedService.sendNext(_this.sharedService.eventTypes.actions.addbasket, true);
                _this.sharedService.saveBasket();
            }
        });
    };
    BasketOneComponent.prototype.editCol = function (item, index) {
        var _this = this;
        var options = {
            viewContainerRef: this.vcRef,
            context: this.sharedService.basket.products[index].product_count,
            fullscreen: false
        };
        this._modalService.showModal(components_1.ModalCountComponent, options)
            .then(function (count) {
            console.log("+++++++++++++++++++++++++++COUNT", count);
            if (count == 0) {
            }
            else {
                _this.sharedService.basket.products[index].product_count = count;
                _this.sharedService.sendNext(_this.sharedService.eventTypes.actions.addbasket, true);
                _this.basketInfoToProducts();
                _this.sharedService.saveBasket().then(function (s) { }, function (err) { });
            }
        });
    };
    BasketOneComponent.prototype.nextStep = function () {
        var _this = this;
        this.sharedService.saveBasket().then(function (success) {
            setTimeout(function () {
                _this.router.navigate(["basket/two"]);
            }, 200);
        }, function (err) {
            _this.router.navigate(["basket/two"]);
        });
    };
    BasketOneComponent.prototype.goMain = function () {
        var _this = this;
        this.sharedService.saveBasket().then(function (success) {
            _this.router.navigate(["index"], {});
        }, function (err) {
        });
    };
    return BasketOneComponent;
}());
BasketOneComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'basket-one',
        templateUrl: 'basket-one.component.html',
        styleUrls: ['basket-one.css']
    }),
    __metadata("design:paramtypes", [services_1.SharedService,
        page_1.Page,
        router_1.RouterExtensions,
        modal_dialog_1.ModalDialogService,
        core_1.ViewContainerRef])
], BasketOneComponent);
exports.BasketOneComponent = BasketOneComponent;

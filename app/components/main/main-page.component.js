"use strict";
var core_1 = require("@angular/core");
var services_1 = require("../../services");
var page_1 = require("ui/page");
var angular_1 = require("nativescript-telerik-ui/sidedrawer/angular");
var MainPageComponent = (function () {
    function MainPageComponent(apiService, _changeDetectionRef, page, sharedService) {
        this.apiService = apiService;
        this._changeDetectionRef = _changeDetectionRef;
        this.page = page;
        this.sharedService = sharedService;
        this.isDisplayPhoneActivate = false;
    }
    MainPageComponent.prototype.ngAfterViewInit = function () {
        this.drawer = this.drawerComponent.sideDrawer;
        this._changeDetectionRef.detectChanges();
    };
    MainPageComponent.prototype.openDrawer = function () {
        this.drawer.showDrawer();
    };
    MainPageComponent.prototype.ngOnInit = function () {
        if (this.sharedService.user == undefined) {
            this.sharedService.observable
                .subscribe(function (value) {
            });
        }
    };
    return MainPageComponent;
}());
__decorate([
    core_1.ViewChild(angular_1.RadSideDrawerComponent),
    __metadata("design:type", angular_1.RadSideDrawerComponent)
], MainPageComponent.prototype, "drawerComponent", void 0);
MainPageComponent = __decorate([
    core_1.Component({
        selector: "main-page",
        moduleId: module.id,
        templateUrl: "./main-page.component.html",
        styleUrls: ['./main-page.css'],
    }),
    __metadata("design:paramtypes", [services_1.ApiService,
        core_1.ChangeDetectorRef,
        page_1.Page,
        services_1.SharedService])
], MainPageComponent);
exports.MainPageComponent = MainPageComponent;

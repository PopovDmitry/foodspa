import {Component, OnInit, ChangeDetectorRef, ViewChild} from "@angular/core";
// import {ConfigService} from '../../services/config.service';
import { Observable } from 'rxjs/Observable';
import { ApiService, SharedService } from "../../services";
import {Page} from "ui/page";
import { RadSideDrawerComponent, SideDrawerType } from "nativescript-telerik-ui/sidedrawer/angular";


@Component({
    selector: "main-page",
    moduleId: module.id,
    templateUrl: "./main-page.component.html",
    styleUrls: ['./main-page.css'],
    // providers: []
})

export class MainPageComponent implements OnInit {

    // указатель надо ли показывать страницу активации телефона
    private isDisplayPhoneActivate: boolean = false;

    // подключаем сервис конфигурации
    constructor(
        private apiService: ApiService, 
        private _changeDetectionRef: ChangeDetectorRef, 
        private page: Page,
        private sharedService: SharedService
        ) {}

    @ViewChild(RadSideDrawerComponent) public drawerComponent: RadSideDrawerComponent;
    private drawer: SideDrawerType;

    ngAfterViewInit() {
        this.drawer = this.drawerComponent.sideDrawer;
        this._changeDetectionRef.detectChanges();
    }
    public openDrawer() {
        this.drawer.showDrawer();
    }
    // инициализация при загрузке
    ngOnInit(): void {
        // console.log('USER++++++++++++', this.apiService.user)
        if(this.sharedService.user == undefined){
            this.sharedService.observable
                .subscribe((value) =>{
                    // console.log('OBS +++++++++++++++++++++++', value.type);
                    // if(value.type == 'userNotFound'){ // нет еще записи в БД о данном устройстве
                    //     this.isDisplayPhoneActivate = true;                
                    // }

                    // if(value.type == 'user' && value.data.app_valid_sms == false){
                    //     this.isDisplayPhoneActivate = true;                
                    // }

                    // if(value.type == 'categories'){
                        
                    // }
                })
        }
        
        
    }

}
import { Component, OnInit, ViewChild, ChangeDetectorRef, ViewContainerRef, NgZone } from '@angular/core';
import { RouterExtensions } from "nativescript-angular/router";
import { ActivatedRoute } from '@angular/router';
import { Page } from "ui/page";
import { Category, Filter, Product, EventCacheModel } from '../../models';
import { SharedService, ImageService } from '../../services'
import { RadSideDrawerComponent, SideDrawerType } from "nativescript-telerik-ui/sidedrawer/angular";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { ModalCountComponent } from '../../components'
import {find} from "lodash";
import 'rxjs/add/operator/switchMap';


@Component({
    moduleId: module.id,
    templateUrl: 'category.component.html',
    styleUrls: ['category.css']
}) 
export class CategoryComponent implements OnInit {
  
    data: Category;
    filters: Filter[];
    products: Product[];
    counts: number[];
    success: any = {};

    @ViewChild(RadSideDrawerComponent) public drawerComponent: RadSideDrawerComponent;
    private drawer: SideDrawerType;
    private url: string = '/index/category'; // база для редиректа
    listViewHeight: number;
    private displayModal: boolean = true;
    private startFilter = 0; // стартовый фильтр
    private class = '';


    constructor(
        private page: Page,
        private router: RouterExtensions,
        private activatedRoute: ActivatedRoute,
        public sharedService: SharedService,  
        private _changeDetectionRef: ChangeDetectorRef, 
        private _modalService: ModalDialogService,
        private vcRef: ViewContainerRef,
        private ngZone: NgZone,
        private imageService: ImageService,
    ) {  
        this.page.actionBarHidden = true; 
    }

    ngOnInit() {
        this.page.actionBarHidden = true;  
        this.page.backgroundSpanUnderStatusBar = true;
        this.class = (this.sharedService.isIOS) ? 'ios' : 'android'
        // this.listViewHeight = this.sharedService.user.screen_height - 30;
        if(this.sharedService.isAndroid){
            this.listViewHeight = this.sharedService.user.screen_height - 150;
        }else{
            this.listViewHeight = 600; 
        }

        this.activatedRoute.params.subscribe( params => {
            console.log('activatedRoute');
            
            if(params['cat']){
                this.getData(params['cat'])
            }
        })
 
        // открываем меню
        this.sharedService
                .observable
                .filter( v => v.type == this.sharedService.eventTypes.actions.openMenu)
                .subscribe( v => {
                    this.drawer.showDrawer();
                })
     }

     // получение данных по категории
     getData(cat){
            this.data = find(this.sharedService.data, (v) => v.name == cat)

            // получаем изображения и за одно кешиируем их
            // this.cacheImage() 

            let filterName: string = "";
            // если есть фильтры, то фильтруем товары по фильтрам
            if(this.data.filters.length > 0){
                if(this.startFilter > this.data.filters.length){
                    this.startFilter = this.data.filters.length;
                }

                let filter: Filter = this.data.filters[this.startFilter];
                filterName = filter.name   
            }

            this.getProductsByFilter(filterName);
     }

     // кеширование изображений
     cacheImage(){
        let images = this.data.products.map((product: Product) => product.image)
        
        let map = this.data.products.map((product: Product) => {
            return {pid: product.pid, image: product.image}
        })

        this.data.products.forEach((product: Product) => {
            product.image = '~/images/filterLoad.png';
        })
        // let clproductsCopyoned = this.data.products.map(x => Object.assign({}, x));
        

        this.imageService.loadAll(images).then((res: EventCacheModel[]) =>{

            res.forEach((loaded: EventCacheModel) => {
                if(!loaded.is_cache){
                    let copyPr: any = find(map, (x: Product) => x.image == loaded.url)
                    let realPr: Product = find(this.data.products, (x: Product) => x.pid == copyPr.pid)
                    realPr.image = loaded.source;
                }
            });

            this.data.products.forEach((product: Product) => {

            })
        })

     }

     // продукты по фильтрам
     getProductsByFilter(name: string = ""){
        this.ngZone.run(() => {
            this.products = [];
            this.counts = []; // обнуляем колличество
            if(name == ""){
                this.products = this.data.products;
                this.data.products.forEach((v: Product) => {
                    this.counts[v.pid] = 1;
                    this.success[v.pid] = false;
                })
            }else{
                // пробегаем по всем продуктом и находим совпадение по наличию фильтров
                if(this.data.products.length > 0){
                    this.data.products.forEach((v: Product) => {
                        if(v.filters.indexOf(name) > -1){
                            this.products.push(v)
                            this.counts[v.pid] = 1;
                            this.success[v.pid] = false;
                        }
                    })
                }
            }
        })

     }

     // после инициализации
    ngAfterViewInit() {
        this.drawer = this.drawerComponent.sideDrawer;
        this._changeDetectionRef.detectChanges();
    }

    // тап по меню
    onMenuTap(item){ 
        // this.ngZone.run(() => {
            let name = item.name;
            this.drawer.closeDrawer()
            this.getData(name)
            // console.log('re Redirect', name, this.router.router.routerState)
            // console.log(this.router);
            
            // let url = "/index/category/" + name;
            // // this.router.navigateByUrl('/index/category/juice').then(() =>{

            // })
            // this.router.navigate(['/index/category', name, {foo: Math.random()}]).then(() => {
            //     console.log('rrrrrrr'); 
            // })
        // })
        
        // this._redirector(name)
    }

    onIndexTap(){
        this.drawer.closeDrawer()
        let option = {}
        if(this.sharedService.isIOS){
            option = {
                        transition: {
                            name: 'slideRight'
                        }
                    }
        }
        this.router.navigate(['/index'], option)
    }

    openModal(item:Product){
        let options: ModalDialogOptions = {
            viewContainerRef: this.vcRef,
            context: this.counts[item.pid],
            fullscreen: false
        };

        this._modalService.showModal(ModalCountComponent, options)
        .then((count) => {
            if(count != undefined){
                this.counts[item.pid] = count;
            }
            // console.log("+++++++++++++++++++++++++++COUNT", count, typeof count)
            // if(count == 0){
            //     // ничего не добавлять
            // }else{
            //     // добавить ное колличестов
            //     this.counts[item.pid] = count;
            //     // this.sharedService.addBasket(item.pid, count);
            // }
        });
    }
    // перенаправление 
    _redirector(name: string, url: string = this.url){
        // отправляем событие о редиректе
        this.sharedService.sendNext(this.sharedService.eventTypes.actions.redirect, {url:url + '/' + name })
        this.router.navigate(["/index/category/", name]).then(success => {}).catch( error => {});
    }

    // перенаправление на товар
    _redirectDetail(name: string, id: number){
        let url = this.url + '/' + name + '/view/' + id;
        this.sharedService.sendNext(this.sharedService.eventTypes.actions.redirect, {url: url})
        this.router.navigate([url], {});
    }

    // tap по добалвению в карзину
    onAddBasketTap(item){
        this.sharedService.addBasket(item.pid,this.counts[item.pid]);
        
        // отключаем анимацию при доабвлении
        // this.animationInAddBasket(item.pid).then((result) => {
        //     setTimeout(() => {
        //         // сбрасываем счетчик
        //         this.counts[item.pid] = 1;
        //         this.animationOutAddBasket(item.pid).then(() => {})
        //     }, 2000);
        // })
    }

    // анимация сообщения, что товар был добавлен
    animationInAddBasket(id): Promise<boolean>{
        return new Promise((resolve, reject) => {
            let gridId: string = 'grid' + id;
            let successId: string = 'success' + id;

            let grid = this.page.getViewById(gridId);
            let success = this.page.getViewById(successId);

            grid.animate({
                opacity: 0,
                duration: 250,
            }).then(() => {
                success.animate({
                    opacity: 1,
                    duration: 250,
                }).then(() => {
                    resolve(true)
                })
            })
        })
    }

    animationOutAddBasket(id): Promise<boolean>{
        return new Promise((resolve, reject) => {
            let gridId: string = 'grid' + id;
            let successId: string = 'success' + id;
            
            let grid = this.page.getViewById(gridId);
            let success = this.page.getViewById(successId);

            success.animate({
                opacity: 0,
                duration: 250,
            }).then(() => {
                grid.animate({
                    opacity: 1,
                    duration: 250,
                }).then(() => {
                    resolve(true)
                })
            })
        })
    }

    // tap переход в тавар
    onItemTab(catalog_name, product_id){ 
        this._redirectDetail(catalog_name, product_id);
    }

    // ставим активный фильтр
    setCurrentFilter(filter: Filter){
        // console.log('ACTIVE FILTER', filter.name)
        this.getProductsByFilter(filter.name)
        // console.dump(filter)
    }

    ngOnDestroy(){
        console.log('ngOnDestroy')
    }

   
}
"use strict";
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var router_2 = require("@angular/router");
var page_1 = require("ui/page");
var services_1 = require("../../services");
var angular_1 = require("nativescript-telerik-ui/sidedrawer/angular");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var components_1 = require("../../components");
var lodash_1 = require("lodash");
require("rxjs/add/operator/switchMap");
var CategoryComponent = (function () {
    function CategoryComponent(page, router, activatedRoute, sharedService, _changeDetectionRef, _modalService, vcRef, ngZone, imageService) {
        this.page = page;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.sharedService = sharedService;
        this._changeDetectionRef = _changeDetectionRef;
        this._modalService = _modalService;
        this.vcRef = vcRef;
        this.ngZone = ngZone;
        this.imageService = imageService;
        this.success = {};
        this.url = '/index/category';
        this.displayModal = true;
        this.startFilter = 0;
        this.class = '';
        this.page.actionBarHidden = true;
    }
    CategoryComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.page.actionBarHidden = true;
        this.page.backgroundSpanUnderStatusBar = true;
        this.class = (this.sharedService.isIOS) ? 'ios' : 'android';
        if (this.sharedService.isAndroid) {
            this.listViewHeight = this.sharedService.user.screen_height - 150;
        }
        else {
            this.listViewHeight = 600;
        }
        this.activatedRoute.params.subscribe(function (params) {
            console.log('activatedRoute');
            if (params['cat']) {
                _this.getData(params['cat']);
            }
        });
        this.sharedService
            .observable
            .filter(function (v) { return v.type == _this.sharedService.eventTypes.actions.openMenu; })
            .subscribe(function (v) {
            _this.drawer.showDrawer();
        });
    };
    CategoryComponent.prototype.getData = function (cat) {
        this.data = lodash_1.find(this.sharedService.data, function (v) { return v.name == cat; });
        var filterName = "";
        if (this.data.filters.length > 0) {
            if (this.startFilter > this.data.filters.length) {
                this.startFilter = this.data.filters.length;
            }
            var filter_1 = this.data.filters[this.startFilter];
            filterName = filter_1.name;
        }
        this.getProductsByFilter(filterName);
    };
    CategoryComponent.prototype.cacheImage = function () {
        var _this = this;
        var images = this.data.products.map(function (product) { return product.image; });
        var map = this.data.products.map(function (product) {
            return { pid: product.pid, image: product.image };
        });
        this.data.products.forEach(function (product) {
            product.image = '~/images/filterLoad.png';
        });
        this.imageService.loadAll(images).then(function (res) {
            res.forEach(function (loaded) {
                if (!loaded.is_cache) {
                    var copyPr_1 = lodash_1.find(map, function (x) { return x.image == loaded.url; });
                    var realPr = lodash_1.find(_this.data.products, function (x) { return x.pid == copyPr_1.pid; });
                    realPr.image = loaded.source;
                }
            });
            _this.data.products.forEach(function (product) {
            });
        });
    };
    CategoryComponent.prototype.getProductsByFilter = function (name) {
        var _this = this;
        if (name === void 0) { name = ""; }
        this.ngZone.run(function () {
            _this.products = [];
            _this.counts = [];
            if (name == "") {
                _this.products = _this.data.products;
                _this.data.products.forEach(function (v) {
                    _this.counts[v.pid] = 1;
                    _this.success[v.pid] = false;
                });
            }
            else {
                if (_this.data.products.length > 0) {
                    _this.data.products.forEach(function (v) {
                        if (v.filters.indexOf(name) > -1) {
                            _this.products.push(v);
                            _this.counts[v.pid] = 1;
                            _this.success[v.pid] = false;
                        }
                    });
                }
            }
        });
    };
    CategoryComponent.prototype.ngAfterViewInit = function () {
        this.drawer = this.drawerComponent.sideDrawer;
        this._changeDetectionRef.detectChanges();
    };
    CategoryComponent.prototype.onMenuTap = function (item) {
        var name = item.name;
        this.drawer.closeDrawer();
        this.getData(name);
    };
    CategoryComponent.prototype.onIndexTap = function () {
        this.drawer.closeDrawer();
        var option = {};
        if (this.sharedService.isIOS) {
            option = {
                transition: {
                    name: 'slideRight'
                }
            };
        }
        this.router.navigate(['/index'], option);
    };
    CategoryComponent.prototype.openModal = function (item) {
        var _this = this;
        var options = {
            viewContainerRef: this.vcRef,
            context: this.counts[item.pid],
            fullscreen: false
        };
        this._modalService.showModal(components_1.ModalCountComponent, options)
            .then(function (count) {
            if (count != undefined) {
                _this.counts[item.pid] = count;
            }
        });
    };
    CategoryComponent.prototype._redirector = function (name, url) {
        if (url === void 0) { url = this.url; }
        this.sharedService.sendNext(this.sharedService.eventTypes.actions.redirect, { url: url + '/' + name });
        this.router.navigate(["/index/category/", name]).then(function (success) { }).catch(function (error) { });
    };
    CategoryComponent.prototype._redirectDetail = function (name, id) {
        var url = this.url + '/' + name + '/view/' + id;
        this.sharedService.sendNext(this.sharedService.eventTypes.actions.redirect, { url: url });
        this.router.navigate([url], {});
    };
    CategoryComponent.prototype.onAddBasketTap = function (item) {
        this.sharedService.addBasket(item.pid, this.counts[item.pid]);
    };
    CategoryComponent.prototype.animationInAddBasket = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var gridId = 'grid' + id;
            var successId = 'success' + id;
            var grid = _this.page.getViewById(gridId);
            var success = _this.page.getViewById(successId);
            grid.animate({
                opacity: 0,
                duration: 250,
            }).then(function () {
                success.animate({
                    opacity: 1,
                    duration: 250,
                }).then(function () {
                    resolve(true);
                });
            });
        });
    };
    CategoryComponent.prototype.animationOutAddBasket = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var gridId = 'grid' + id;
            var successId = 'success' + id;
            var grid = _this.page.getViewById(gridId);
            var success = _this.page.getViewById(successId);
            success.animate({
                opacity: 0,
                duration: 250,
            }).then(function () {
                grid.animate({
                    opacity: 1,
                    duration: 250,
                }).then(function () {
                    resolve(true);
                });
            });
        });
    };
    CategoryComponent.prototype.onItemTab = function (catalog_name, product_id) {
        this._redirectDetail(catalog_name, product_id);
    };
    CategoryComponent.prototype.setCurrentFilter = function (filter) {
        this.getProductsByFilter(filter.name);
    };
    CategoryComponent.prototype.ngOnDestroy = function () {
        console.log('ngOnDestroy');
    };
    return CategoryComponent;
}());
__decorate([
    core_1.ViewChild(angular_1.RadSideDrawerComponent),
    __metadata("design:type", angular_1.RadSideDrawerComponent)
], CategoryComponent.prototype, "drawerComponent", void 0);
CategoryComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: 'category.component.html',
        styleUrls: ['category.css']
    }),
    __metadata("design:paramtypes", [page_1.Page,
        router_1.RouterExtensions,
        router_2.ActivatedRoute,
        services_1.SharedService,
        core_1.ChangeDetectorRef,
        modal_dialog_1.ModalDialogService,
        core_1.ViewContainerRef,
        core_1.NgZone,
        services_1.ImageService])
], CategoryComponent);
exports.CategoryComponent = CategoryComponent;

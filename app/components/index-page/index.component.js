"use strict";
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var services_1 = require("../../services");
var page_1 = require("ui/page");
var angular_1 = require("nativescript-telerik-ui/sidedrawer/angular");
var IndexComponent = (function () {
    function IndexComponent(sharedService, page, router, _changeDetectionRef) {
        this.sharedService = sharedService;
        this.page = page;
        this.router = router;
        this._changeDetectionRef = _changeDetectionRef;
        this.url = '/index/category';
    }
    IndexComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.page.actionBarHidden = true;
        this.listViewHeight = this.sharedService.user.screen_height - 30;
        this.page.backgroundSpanUnderStatusBar = true;
        this.sharedService
            .observable
            .filter(function (v) { return v.type == _this.sharedService.eventTypes.actions.openMenu; })
            .subscribe(function (v) {
            _this.drawer.showDrawer();
        });
    };
    IndexComponent.prototype.ngAfterViewInit = function () {
        this.drawer = this.drawerComponent.sideDrawer;
        this._changeDetectionRef.detectChanges();
    };
    IndexComponent.prototype.onItemTap = function (index) {
        var name = this.sharedService.data[index].name;
        this._redirect(name);
    };
    IndexComponent.prototype.onMenuTap = function (item) {
        var name = item.name;
        this.drawer.closeDrawer();
        this._redirect(name);
    };
    IndexComponent.prototype._redirect = function (name, url) {
        if (url === void 0) { url = this.url; }
        this.sharedService.sendNext(this.sharedService.eventTypes.actions.redirect, { url: url + name });
        var options = {};
        this.router.navigate(["/index/category", name], options);
    };
    IndexComponent.prototype.openDrawer = function () {
        this.drawer.showDrawer();
    };
    return IndexComponent;
}());
__decorate([
    core_1.ViewChild(angular_1.RadSideDrawerComponent),
    __metadata("design:type", angular_1.RadSideDrawerComponent)
], IndexComponent.prototype, "drawerComponent", void 0);
IndexComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: 'index.component.html',
        styleUrls: ['./index.css'],
    }),
    __metadata("design:paramtypes", [services_1.SharedService,
        page_1.Page,
        router_1.RouterExtensions,
        core_1.ChangeDetectorRef])
], IndexComponent);
exports.IndexComponent = IndexComponent;

import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { RouterExtensions } from "nativescript-angular/router";
import { SharedService } from '../../services'
import { Page } from "ui/page";
import { RadSideDrawerComponent, SideDrawerType } from "nativescript-telerik-ui/sidedrawer/angular";

@Component({
    moduleId: module.id,
    templateUrl: 'index.component.html',
    styleUrls: ['./index.css'],
})

export class IndexComponent implements OnInit {
    listViewHeight: number;

    @ViewChild(RadSideDrawerComponent) public drawerComponent: RadSideDrawerComponent;
    private drawer: SideDrawerType;

    private url: string = '/index/category'; // база для редиректа
    // private imageSrc: any;
    // private tst: boolean = false;
    // public test: string = 'http://foodspa.ru:9090/uploads/products/dbfd51a1707229677bdcf16db50d2f91.jpg'
 
    constructor( 
        public sharedService: SharedService,
        private page: Page,
        private router: RouterExtensions,
        private _changeDetectionRef: ChangeDetectorRef, 
    ) { }

    ngOnInit() { 
        this.page.actionBarHidden = true;  
        this.listViewHeight = this.sharedService.user.screen_height - 30;
        this.page.backgroundSpanUnderStatusBar = true;

        // this.imageSrc = this.imageService.cache.placeholder;
        // this.imageService.observable.subscribe((v) => {
        //     this.imageSrc = v.source;
        //     this.tst = v.is_cache
        // })
        // this.imageService.imageSrc('')

        // открываем меню 
        this.sharedService
                .observable
                .filter( v => v.type == this.sharedService.eventTypes.actions.openMenu)
                .subscribe( v => {
                    this.drawer.showDrawer();
                })
                
    }

    // после инициализации
    ngAfterViewInit() {
        this.drawer = this.drawerComponent.sideDrawer;
        this._changeDetectionRef.detectChanges();
    }

    // если тапнули по категории
    onItemTap(index){
        let name = this.sharedService.data[index].name;
        this._redirect(name)
    }

    // тап по меню
    onMenuTap(item){
        let name = item.name;
        this.drawer.closeDrawer()
        this._redirect(name)
    }

    // перенаправление 
    _redirect(name: string, url: string = this.url){
        // отправляем событие о релиректе
        this.sharedService.sendNext(this.sharedService.eventTypes.actions.redirect, {url:url + name })
        let options = {} 
        this.router.navigate(["/index/category", name], options);
    }

    // метод открытия бокового меню
    public openDrawer() {
        this.drawer.showDrawer();
    }

}
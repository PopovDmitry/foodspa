// подключаем компоненты и интрефейс начало загрузки
import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { Page } from "ui/page";
import { TextField } from "ui/text-field";
import { Response } from '@angular/http';
import { RouterExtensions } from "nativescript-angular/router";
import { Device } from '../../models/';
import dialogs = require("ui/dialogs");
import platformModule = require("platform");
import { ApiService, SharedService } from "../../services";

@Component({
    moduleId: module.id,
    templateUrl: './activation.html',
    styleUrls: ['./activation.css']
})

export class ActivationComponent implements OnInit{
    private user: any;

    constructor(
        private page: Page,
        private router: RouterExtensions,
        private apiService: ApiService,
        private sharedService: SharedService
        ){
        // прячем actionBAR для андроида и на всякий для IOS
        
    }
 
    // доступ к элементу #phoneNumber
    @ViewChild("phoneNumber") phoneText: ElementRef;

    // инициализация при загрузке
    ngOnInit(): void{
        // ставим фокус на поле ввода
        this.phoneText.nativeElement.focus = true;

        this.page.actionBarHidden = true;
        this.page.backgroundSpanUnderStatusBar = true;        

        // тупо но влияет нерстку
        this.phoneText.nativeElement.text = "1";
        this.phoneText.nativeElement.text = "";

        if(this.sharedService.user != undefined || this.sharedService.user != null){
            this.phoneText.nativeElement.text = this.sharedService.user.app_phone.toString();
        }
    }

    // обработка события по нажатию на кнопке о высалке кода
    onNext(){
        // получаем номер и очищаем его от лишних символов
        var phone = this.phoneText.nativeElement.text.replace(/[^0-9.]/g, "");
        if(phone.length != 10){
            this.showAlert('Длина введеного вами номера телефона не соотвествует нужной')
        }else{
            let device = new Device(
                phone, 
                true,
                1111,
                false, 
                platformModule.device.os,
                platformModule.device.osVersion,
                platformModule.device.sdkVersion,
                platformModule.device.model,
                platformModule.device.deviceType,
                platformModule.screen.mainScreen.widthDIPs,
                platformModule.screen.mainScreen.heightDIPs,
                platformModule.device.uuid,
                false
             )

             

             // проверяем был ли зарегистрован данный номер
             this.apiService.registrationDevice(device).subscribe( v => {
                    if(v.result){
                        this.sharedService.user = device;
                        this.router.navigate(["/activation/getCode"], {
                            transition: {
                                name: 'slideLeft'
                            }
                        });
                    }else{
                        // ошибка сервера
                    }
                },
                error => {
                    console.dump(error);
                }
             )

             // обновляем данные
            //  this.apiService.setValueByUser('/users', user).then(success => {
            //      // TODO отправка смс

            //      this.router.navigate(["/activation/getCode"], {
            //         transition: {
            //             name: 'slideLeft'
            //         }
            //      });
            //  })
        }
    }

    // выводит алерт и по необходисости вызывает функцию
    showAlert(msg: string, callback = ()=>{}){
        dialogs.alert(msg).then(callback);
    }

}
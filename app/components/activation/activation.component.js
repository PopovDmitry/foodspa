"use strict";
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var router_1 = require("nativescript-angular/router");
var _1 = require("../../models/");
var dialogs = require("ui/dialogs");
var platformModule = require("platform");
var services_1 = require("../../services");
var ActivationComponent = (function () {
    function ActivationComponent(page, router, apiService, sharedService) {
        this.page = page;
        this.router = router;
        this.apiService = apiService;
        this.sharedService = sharedService;
    }
    ActivationComponent.prototype.ngOnInit = function () {
        this.phoneText.nativeElement.focus = true;
        this.page.actionBarHidden = true;
        this.page.backgroundSpanUnderStatusBar = true;
        this.phoneText.nativeElement.text = "1";
        this.phoneText.nativeElement.text = "";
        if (this.sharedService.user != undefined || this.sharedService.user != null) {
            this.phoneText.nativeElement.text = this.sharedService.user.app_phone.toString();
        }
    };
    ActivationComponent.prototype.onNext = function () {
        var _this = this;
        var phone = this.phoneText.nativeElement.text.replace(/[^0-9.]/g, "");
        if (phone.length != 10) {
            this.showAlert('Длина введеного вами номера телефона не соотвествует нужной');
        }
        else {
            var device_1 = new _1.Device(phone, true, 1111, false, platformModule.device.os, platformModule.device.osVersion, platformModule.device.sdkVersion, platformModule.device.model, platformModule.device.deviceType, platformModule.screen.mainScreen.widthDIPs, platformModule.screen.mainScreen.heightDIPs, platformModule.device.uuid, false);
            this.apiService.registrationDevice(device_1).subscribe(function (v) {
                if (v.result) {
                    _this.sharedService.user = device_1;
                    _this.router.navigate(["/activation/getCode"], {
                        transition: {
                            name: 'slideLeft'
                        }
                    });
                }
                else {
                }
            }, function (error) {
                console.dump(error);
            });
        }
    };
    ActivationComponent.prototype.showAlert = function (msg, callback) {
        if (callback === void 0) { callback = function () { }; }
        dialogs.alert(msg).then(callback);
    };
    return ActivationComponent;
}());
__decorate([
    core_1.ViewChild("phoneNumber"),
    __metadata("design:type", core_1.ElementRef)
], ActivationComponent.prototype, "phoneText", void 0);
ActivationComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: './activation.html',
        styleUrls: ['./activation.css']
    }),
    __metadata("design:paramtypes", [page_1.Page,
        router_1.RouterExtensions,
        services_1.ApiService,
        services_1.SharedService])
], ActivationComponent);
exports.ActivationComponent = ActivationComponent;

"use strict";
var core_1 = require("@angular/core");
var MenuComponent = (function () {
    function MenuComponent() {
    }
    MenuComponent.prototype.ngOnInit = function () {
    };
    return MenuComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Array)
], MenuComponent.prototype, "categories", void 0);
MenuComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'Menu',
        templateUrl: 'menu.component.html'
    }),
    __metadata("design:paramtypes", [])
], MenuComponent);
exports.MenuComponent = MenuComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVudS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJtZW51LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsc0NBQXlEO0FBUXpELElBQWEsYUFBYTtJQUd0QjtJQUFnQixDQUFDO0lBRWpCLGdDQUFRLEdBQVI7SUFFQSxDQUFDO0lBQ0wsb0JBQUM7QUFBRCxDQUFDLEFBUkQsSUFRQztBQVBZO0lBQVIsWUFBSyxFQUFFOztpREFBd0I7QUFEdkIsYUFBYTtJQUx6QixnQkFBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1FBQ25CLFFBQVEsRUFBRSxNQUFNO1FBQ2hCLFdBQVcsRUFBRSxxQkFBcUI7S0FDckMsQ0FBQzs7R0FDVyxhQUFhLENBUXpCO0FBUlksc0NBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENhdGVnb3J5IH0gZnJvbSAnLi4vLi4vbW9kZWxzJztcblxuQENvbXBvbmVudCh7XG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbiAgICBzZWxlY3RvcjogJ01lbnUnLFxuICAgIHRlbXBsYXRlVXJsOiAnbWVudS5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgTWVudUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgQElucHV0KCkgY2F0ZWdvcmllczogQ2F0ZWdvcnlbXTtcblxuICAgIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgICBuZ09uSW5pdCgpIHsgXG4gICAgICAgIFxuICAgIH1cbn0iXX0=
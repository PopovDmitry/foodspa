"use strict";
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var router_2 = require("@angular/router");
var services_1 = require("../../services");
var page_1 = require("ui/page");
var lodash_1 = require("lodash");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var components_1 = require("../../components");
var DetailComponent = (function () {
    function DetailComponent(page, router, sharedService, activatedRoute, _modalService, vcRef) {
        this.page = page;
        this.router = router;
        this.sharedService = sharedService;
        this.activatedRoute = activatedRoute;
        this._modalService = _modalService;
        this.vcRef = vcRef;
        this.displayFeatures = false;
        this.count = 1;
        this.success = false;
    }
    DetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.page.actionBarHidden = true;
        this.page.backgroundSpanUnderStatusBar = true;
        this.listViewHeight = this.sharedService.user.screen_height - 310;
        this.activatedRoute.params.subscribe(function (params) {
            _this.params = params;
            if (params['cat']) {
                _this.backUrl = 'index/category/' + params['cat'];
                _this.data = lodash_1.find(_this.sharedService.data, function (v) { return v.name == params['cat']; });
                _this.products = _this.data.products;
                _this.item = lodash_1.find(_this.products, function (v) { return v.pid == params['id']; });
            }
        });
    };
    DetailComponent.prototype.openModal = function () {
        var _this = this;
        var options = {
            viewContainerRef: this.vcRef,
            context: this.count,
            fullscreen: false
        };
        this._modalService.showModal(components_1.ModalCountComponent, options)
            .then(function (count) {
            if (count != undefined) {
                _this.count = count;
            }
        });
    };
    DetailComponent.prototype.toogleFeatures = function () {
        this.displayFeatures = !this.displayFeatures;
    };
    DetailComponent.prototype.ngAfterViewInit = function () {
    };
    DetailComponent.prototype.onAddBasketTap = function (item) {
        this.sharedService.addBasket(item.pid, this.count);
    };
    DetailComponent.prototype.animationInAddBasket = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var grid = _this.page.getViewById('grid');
            var success = _this.page.getViewById('success');
            grid.animate({
                opacity: 0,
                duration: 250,
            }).then(function () {
                _this.success = true;
                success.animate({
                    opacity: 1,
                    duration: 250,
                }).then(function () {
                    resolve(true);
                });
            });
        });
    };
    DetailComponent.prototype.animationOutAddBasket = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var grid = _this.page.getViewById('grid');
            var success = _this.page.getViewById('success');
            success.animate({
                opacity: 0,
                duration: 250,
            }).then(function () {
                _this.success = false;
                grid.animate({
                    opacity: 1,
                    duration: 250,
                }).then(function () {
                    resolve(true);
                });
            });
        });
    };
    return DetailComponent;
}());
__decorate([
    core_1.ViewChild("slides"),
    __metadata("design:type", Object)
], DetailComponent.prototype, "slides", void 0);
DetailComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'detail',
        templateUrl: 'detail.component.html',
        styleUrls: ['detail.css']
    }),
    __metadata("design:paramtypes", [page_1.Page,
        router_1.RouterExtensions,
        services_1.SharedService,
        router_2.ActivatedRoute,
        modal_dialog_1.ModalDialogService,
        core_1.ViewContainerRef])
], DetailComponent);
exports.DetailComponent = DetailComponent;

import { Component, OnInit, ElementRef, ViewChild, ViewContainerRef } from '@angular/core';
import { RouterExtensions } from "nativescript-angular/router";
import { ActivatedRoute } from '@angular/router';
import { SharedService } from '../../services';
import { Page } from "ui/page";
import { Category, Filter, Product } from '../../models';
import {find} from "lodash";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { ModalCountComponent } from '../../components'

@Component({
    moduleId: module.id,
    selector: 'detail',
    templateUrl: 'detail.component.html',
    styleUrls: ['detail.css']
})
export class DetailComponent implements OnInit {
    
    listViewHeight: number;
    data: Category;
    filters: Filter[];
    products: Product[];
    item: Product;
    params: any;
    startIndex: number;
    @ViewChild("slides") slides;
    displayFeatures: boolean = false;
    backUrl: string;
    count: number = 1; // количество товаров для доабвления
    success: boolean = false;
    

    constructor(
        private page: Page,
        private router: RouterExtensions,
        public sharedService: SharedService, 
        private activatedRoute: ActivatedRoute,
        private _modalService: ModalDialogService,
        private vcRef: ViewContainerRef,
    ) {  }

    ngOnInit() { 
        this.page.actionBarHidden = true;  
        this.page.backgroundSpanUnderStatusBar = true;
        this.listViewHeight = this.sharedService.user.screen_height - 310;

        this.activatedRoute.params.subscribe( params => {
            this.params = params;
            if(params['cat']){
                this.backUrl = 'index/category/' + params['cat'];
                this.data = find(this.sharedService.data, (v) => v.name == params['cat'])
                this.products = this.data.products;
                // вылавливаем продукт
                this.item = find(this.products, (v) => v.pid == params['id'])
            }
        })
    }

    openModal(){
        let options: ModalDialogOptions = {
            viewContainerRef: this.vcRef,
            context: this.count,
            fullscreen: false
        };

        this._modalService.showModal(ModalCountComponent, options)
        .then((count) => {
            if(count != undefined){
                this.count = count;
            }
        });
    }

    // "полезные свойства"
    toogleFeatures(){
        this.displayFeatures = !this.displayFeatures;
    }


    // запуск функции после рендера
    ngAfterViewInit(){ 
        // this.router.router.ngOnDestroy();
    }

    // tap по добалвению в карзину
    onAddBasketTap(item: Product){
        this.sharedService.addBasket(item.pid,this.count);


        // анимация добавления отключено
        // this.animationInAddBasket().then(() => {
        //     this.count = 1;
        //     setTimeout(() => {
        //         this.animationOutAddBasket().then(() => {

        //         })
        //     }, 2000)
        // })
    }

    // анимация сообщения, что товар был добавлен
    animationInAddBasket(): Promise<boolean>{
        return new Promise((resolve, reject) => {
            let grid = this.page.getViewById('grid');
            let success = this.page.getViewById('success');
            grid.animate({
                opacity: 0,
                duration: 250,
            }).then(() => {
                this.success = true;
                success.animate({
                    opacity: 1,
                    duration: 250,
                }).then(() => {
                    resolve(true)
                })
            })
        })
    }

    // анимация прятания сообщения, что товар был добавлен
    animationOutAddBasket(): Promise<boolean>{
        return new Promise((resolve, reject) => {
            let grid = this.page.getViewById('grid');
            let success = this.page.getViewById('success');
            success.animate({
                opacity: 0,
                duration: 250,
            }).then(() => {
                this.success = false;
                grid.animate({
                    opacity: 1,
                    duration: 250,
                }).then(() => {
                    resolve(true)
                })
            })
        })
    }
}
"use strict";
var core_1 = require("@angular/core");
var services_1 = require("../../services");
var router_1 = require("nativescript-angular/router");
var dialogs = require("ui/dialogs");
var page_1 = require("ui/page");
var MyActionBarComponent = (function () {
    function MyActionBarComponent(sharedService, router, page) {
        this.sharedService = sharedService;
        this.router = router;
        this.page = page;
        this.title = "Food SPA";
        this.back = false;
        this.backUrl = "";
        this.basket = true;
        this.className = (this.sharedService.isIOS == true) ? 'iosBar' : 'androidBar';
        this.basketCount = 0;
    }
    MyActionBarComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.className = this.className + " myBar";
        this.reCount();
        this.sharedService
            .observable
            .filter(function (v) { return v.type == _this.sharedService.eventTypes.actions.addbasket; })
            .subscribe(function (v) {
            _this.reCount();
        });
    };
    MyActionBarComponent.prototype.ngOpenMenu = function ($event) {
        this.sharedService.sendNext(this.sharedService.eventTypes.actions.openMenu, $event);
    };
    MyActionBarComponent.prototype.reCount = function () {
        if (this.sharedService.basket != undefined && this.sharedService.basket.products != undefined) {
            this.basketCount = this.sharedService.basket.products.reduce(function (count, current) {
                return count + current.product_count;
            }, 0);
        }
    };
    MyActionBarComponent.prototype.goToBasket = function () {
        if (this.basketCount == 0) {
            dialogs.alert('Товаров нет в корзине').then(function () { });
        }
        else {
            this.router.navigate(['/basket']);
        }
    };
    MyActionBarComponent.prototype.ngBack = function () {
        if (this.backUrl != "") {
            console.log('NAVIGATE', this.backUrl, this.sharedService.isIOS);
            this.router.navigate([this.backUrl]);
        }
        else {
            this.router.back();
        }
    };
    return MyActionBarComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], MyActionBarComponent.prototype, "title", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], MyActionBarComponent.prototype, "back", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], MyActionBarComponent.prototype, "backUrl", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], MyActionBarComponent.prototype, "basket", void 0);
MyActionBarComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'my-action-bar',
        templateUrl: 'myactionBar.component.html',
        styleUrls: ['myActionBar.css']
    }),
    __metadata("design:paramtypes", [services_1.SharedService,
        router_1.RouterExtensions,
        page_1.Page])
], MyActionBarComponent);
exports.MyActionBarComponent = MyActionBarComponent;

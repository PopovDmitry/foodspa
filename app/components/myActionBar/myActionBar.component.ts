import { Component, OnInit, Input } from '@angular/core';
import application = require("application");
import { SharedService } from '../../services';
import { RouterExtensions } from "nativescript-angular/router";
import dialogs = require("ui/dialogs");
import { Page } from "ui/page";

@Component({
    moduleId: module.id,
    selector: 'my-action-bar',
    templateUrl: 'myactionBar.component.html',
    styleUrls: ['myActionBar.css']
})
export class MyActionBarComponent implements OnInit {

    @Input() title: string = "Food SPA";
    @Input() back: boolean = false;
    @Input() backUrl: string = "";
    @Input() basket: boolean = true;
    public className: string = (this.sharedService.isIOS == true) ? 'iosBar' : 'androidBar';
    public basketCount: number = 0;


    constructor(
        private sharedService: SharedService,
        private router: RouterExtensions,
        private page: Page,
    ) {}
    
    ngOnInit() {
        this.className = this.className + " myBar"
        this.reCount();
        this.sharedService
            .observable
            .filter( v => v.type == this.sharedService.eventTypes.actions.addbasket)
            .subscribe( v=> {
                 this.reCount()
            })            
            
    }

    // открыть меню
    ngOpenMenu($event): void{
        this.sharedService.sendNext(this.sharedService.eventTypes.actions.openMenu, $event)
    }

    reCount(){
        
        if(this.sharedService.basket != undefined && this.sharedService.basket.products != undefined){
            // анимация 
            //badgetId
            

            // if(this.basket){
            //     let badget = this.page.getViewById('badgetId');
            //     badget.animate({
            //         // opacity: 0.5,
            //         translate: { x: 0, y: 3 },
            //         duration: 200,
            //     }).then(() => {
            //         badget.animate({
            //             translate: { x: 0, y: 0 },
            //             opacity: 1,
            //             duration: 200,
            //         }).then(() => {

            //         })
            //     })
            // }

            


            this.basketCount = this.sharedService.basket.products.reduce((count, current) => {
                return count + current.product_count;
            }, 0)
        }
        
    }

    goToBasket(){
        if(this.basketCount == 0){
            dialogs.alert('Товаров нет в корзине').then(() => {});
        }else{
           this.router.navigate(['/basket']);  
        }
    }

    // вернутся назад
    ngBack(){
        if(this.backUrl != ""){
            console.log('NAVIGATE', this.backUrl, this.sharedService.isIOS)
            // let option = {}
            // if(this.sharedService.isIOS){
            //     option = {
            //         transition: {
            //             name: 'slideLeft'
            //         }
            //     }
            // }
            
            this.router.navigate([this.backUrl]); 
        }else{
            this.router.back(); 
        }
        
        // this.router.backToPreviousPage() 
    }
}
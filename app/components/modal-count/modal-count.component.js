"use strict";
var core_1 = require("@angular/core");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var page_1 = require("ui/page");
var ModalCountComponent = (function () {
    function ModalCountComponent(params, page) {
        this.params = params;
        this.page = page;
    }
    ModalCountComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
        this.page.backgroundSpanUnderStatusBar = true;
    };
    ModalCountComponent.prototype.onPlus = function () {
        this.params.context++;
    };
    ModalCountComponent.prototype.onMinus = function () {
        if (this.params.context != 1) {
            this.params.context--;
        }
    };
    ModalCountComponent.prototype.closeAdd = function () {
        this.params.closeCallback(this.params.context);
    };
    ModalCountComponent.prototype.close = function () {
        this.params.closeCallback(0);
    };
    return ModalCountComponent;
}());
ModalCountComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: 'modal-count.component.html',
        styleUrls: ['modal-count.css']
    }),
    __metadata("design:paramtypes", [modal_dialog_1.ModalDialogParams, page_1.Page])
], ModalCountComponent);
exports.ModalCountComponent = ModalCountComponent;

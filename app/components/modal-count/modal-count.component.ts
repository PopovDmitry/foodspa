import { Component, OnInit, NgModule } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { DatePicker } from "ui/date-picker";
import { Page } from "ui/page";


@Component({
    moduleId: module.id,
    // selector: 'modal-count',
    templateUrl: 'modal-count.component.html',
    styleUrls: ['modal-count.css']
})
export class ModalCountComponent implements OnInit {
    constructor(private params: ModalDialogParams, private page: Page) { }

    ngOnInit() { 
        // this.page.backgroundColor = "transparent";
        this.page.actionBarHidden = true;  
        this.page.backgroundSpanUnderStatusBar = true;
        // console.log(this.params.context);
    }

    onPlus(){
        this.params.context++;
    }

    onMinus(){
        if(this.params.context != 1){
            this.params.context--;
        }
    }

    closeAdd(){
        this.params.closeCallback(this.params.context)
    }

    close(){
        this.params.closeCallback(0)
    }
}
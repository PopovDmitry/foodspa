import { Component, OnInit } from '@angular/core';
import { Page } from "ui/page";
import { Router, NavigationExtras } from "@angular/router";
import { ApiService, SharedService } from "../../services";
import { RouterExtensions } from "nativescript-angular/router";
var TnsOneSignal = require('nativescript-onesignal').TnsOneSignal;
import * as application from 'application';
var utils = require("utils/utils");

@Component({
    moduleId: module.id,
    selector: 'push',
    templateUrl: 'push.component.html',
    styleUrls: ['push.css']
})

export class PushComponent implements OnInit {
    private push: boolean = false;
    constructor(
        private page: Page,  
        private router: RouterExtensions, 
        private apiService: ApiService,
        private sharedService: SharedService
    ) { }

    ngOnInit() { 
        this.page.actionBarHidden = true;
        this.page.backgroundSpanUnderStatusBar = true;
        this.push = this._hasPermission();
        if(this.push == true){
            this.finish();
        }

        
    }

    allowPush(){
        if(this.sharedService.isIOS){
            TnsOneSignal.registerForPushNotifications();
        }
        // console.log('_hasPermission', this._hasPermission());

        // TnsOneSignal.registerForPushNotifications();
        // TnsOneSignal.setSubscription(true);
        // TnsOneSignal.IdsAvailable(function(userId, registrationId) {
        //     console.log("+++++++++++++++++++++trying to get IDS...", userId);
        // })
    }

    finish(){
        this.router.navigate(['loading'], {clearHistory: true, }).then( s => {})
    }



    _hasPermission () {
        var app = utils.ios.getter(UIApplication, UIApplication.alloc);
        var settings = utils.ios.getter(app, app.currentUserNotificationSettings);
        return (settings.types) > 0;
     };
}
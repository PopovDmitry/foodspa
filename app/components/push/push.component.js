"use strict";
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var services_1 = require("../../services");
var router_1 = require("nativescript-angular/router");
var TnsOneSignal = require('nativescript-onesignal').TnsOneSignal;
var utils = require("utils/utils");
var PushComponent = (function () {
    function PushComponent(page, router, apiService, sharedService) {
        this.page = page;
        this.router = router;
        this.apiService = apiService;
        this.sharedService = sharedService;
        this.push = false;
    }
    PushComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
        this.page.backgroundSpanUnderStatusBar = true;
        this.push = this._hasPermission();
        if (this.push == true) {
            this.finish();
        }
    };
    PushComponent.prototype.allowPush = function () {
        if (this.sharedService.isIOS) {
            TnsOneSignal.registerForPushNotifications();
        }
    };
    PushComponent.prototype.finish = function () {
        this.router.navigate(['loading'], { clearHistory: true, }).then(function (s) { });
    };
    PushComponent.prototype._hasPermission = function () {
        var app = utils.ios.getter(UIApplication, UIApplication.alloc);
        var settings = utils.ios.getter(app, app.currentUserNotificationSettings);
        return (settings.types) > 0;
    };
    ;
    return PushComponent;
}());
PushComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'push',
        templateUrl: 'push.component.html',
        styleUrls: ['push.css']
    }),
    __metadata("design:paramtypes", [page_1.Page,
        router_1.RouterExtensions,
        services_1.ApiService,
        services_1.SharedService])
], PushComponent);
exports.PushComponent = PushComponent;

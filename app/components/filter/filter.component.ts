import { Component, OnInit, Input, ElementRef, ViewChild, Output, EventEmitter } from '@angular/core';
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "ui/page";
import { Filter, EventCacheModel } from '../../models';
import { SharedService, ImageService } from '../../services';
import { AbsoluteLayout } from 'ui/layouts/absolute-layout';
import { StackLayout } from 'ui/layouts/stack-layout';
import { Label } from 'ui/label';
import { Image } from 'ui/image';
import * as gestures from 'ui/gestures';
import { AnimationCurve, Orientation } from 'ui/enums';
import { find } from "lodash"; 
import { WebImage } from "nativescript-web-image-cache";

@Component({
    moduleId: module.id,
    selector: 'filter',
    templateUrl: 'filter.component.html',
})

// основной компонет
export class FilterComponent implements OnInit {

    // входящие данные
    @Input() filters: Filter[];
    @Input() start: number = 0; // страртовый фильтр (слайд)
    @ViewChild('filters') abs: ElementRef;
    @ViewChild('wraperSlide') wraperSlide: ElementRef;
    @Output() currentFilter = new EventEmitter();
    
    private slideWidth = this.sharedService.user.screen_width / 3;
    private prevDeltaX: number;
    private maxLeft: number;
    private maxRight: number = this.slideWidth;
    private duration: number = 200; // скорость докрутки анимации
    private currentSlide: number = 0; // текущий слайд
    private images: any[] = []
    private l: any[] = [];

    constructor(
        private sharedService: SharedService,
        private router: RouterExtensions,
        private page: Page,
        private imageService: ImageService,
    ) { 
        
    }

    ngOnInit() {
        if(this.start > this.filters.length){ // если стартовый фильтр больше имеющихся
            this.start = this.filters.length // ставим самый последний
        }

        this.currentSlide = this.start; 
    }
    // вызов метода после инициализации View
    ngAfterViewInit(){
        // this.abs.nativeElement.width = 1000;
        this.wraperSlide.nativeElement.width = this.slideWidth * this.filters.length;
        
        // кеширование изображений
        let images = this.filters.map((v: Filter) => v.image )
        this.images = this.filters.map((v: Filter) => '~/images/filterLoad.png' )

        // console.log('this.filters', this.filters.length)
        // console.dump(this.filters);

        // this.imageService.loadAll(images).then((res:EventCacheModel[]) => {            
        //     res.forEach((x: EventCacheModel) => {
        //         let index = images.indexOf(x.url)
        //         if(index != -1){
        //             let wrap = this.abs.nativeElement.getChildAt(index); // обертка в виде StackLayout 
        //             let image: Image = wrap.getChildAt(0); // изображение
        //             image.imageSource = x.source;
        //         }
        //     })
        // })

        this.filters.forEach( (filter: Filter, index: number) => {
            // создаем новую вьюшку
            let slide = new StackLayout();
            slide.width = this.slideWidth;
            // slide.className = "slider"; 
            slide.style.textAlignment = "center";

            // создаем изобаражение
            var image;
            if(this.sharedService.isIOS){
                image = new WebImage()
            }else{
                image = new Image();
            }
            
            // image.placeholder = '~/images/filterLoad.png';
            image.src = filter.image
            // image.src = this.images[index];
            // image.id = 'image' + index;

            image.width = 100;
            image.height = 100;
            image.style.borderRadius = 50;
            image.style.marginBottom = 10;
            image.style.marginTop = image.style.marginBottom

            // создаем текст в слайде
            let label = new Label();
            label.text = filter.title;
            // label.text = 'test';
            label.style.marginBottom = 10;
            
            slide.addChild(image)
            slide.addChild(label)

            // позиционируем вьюшку
            let position = index * this.slideWidth;
            
            // ставим с лева
            AbsoluteLayout.setLeft(slide, position);
            this.abs.nativeElement.addChild(slide)
        })


        // старовое положение
        this.abs.nativeElement.translateX = this.slideWidth;

        // максимальный отступ слева
        this.maxLeft = ((this.filters.length - 2) * this.slideWidth) * -1;

        // активируем старовый слайд
        this.moveToSlide(this.currentSlide); 
        

        // запускаем слушаение события по свайпу
        this.interaction()
        
    }

    // начало взаимодействия
    interaction(){
        this.wraperSlide.nativeElement.on('pan', (args: gestures.PanGestureEventData) => {
            
            // определяем тип событий
            if (args.state === gestures.GestureStateTypes.began) { // начало pan
                this.prevDeltaX = 0;
            } else if (args.state === gestures.GestureStateTypes.ended) { // окончание pan
                
                // если ущли в глубокий плюс, возвращяаем на максимальную глубину
                if(this.abs.nativeElement.translateX > this.maxRight){
                    this.moveToSlide(0)
                    return;
                }

                // если ущли в глубокий минус, возвращяаем на максимальную глубину
                if(this.abs.nativeElement.translateX < this.maxLeft){
                    this.moveToSlide(this.filters.length - 1)
                    return;
                }

                this.currentSlide = this.translateXToSlide(this.abs.nativeElement.translateX);

                // отправляем на нужное положение
                this.moveToSlide(this.currentSlide);
            }else{
                this.setPosition(args.deltaX)

                // начинаем поиск активного слайда на данный момент
                let active: number  = this.translateXToSlide(this.abs.nativeElement.translateX);
                if(active != this.currentSlide){
                    this.setActiveSlide(active);
                    active = null;
                }
                
            }
        })
    }

    // опредеяем слайд по его положению
    translateXToSlide(x): number{

        if(x < this.maxLeft){
            return this.filters.length - 1;
        }

        if(x > this.maxRight){
            return 0; 
        }

        // из максимального левого положения вычетаем текущее положение
        let x1 = Math.round(this.maxLeft - x);

        // делаем положительное число
        if(x1 < 0){
            x1 = x1 * -1;
        }

        // из общей длины надо вычесть x1
        let x2 = (this.filters.length * this.slideWidth) - x1;

        // результат смещения, делим на ширину шага  и округляем до ближайшего целого
        let x3 = Math.round(x2 / this.slideWidth) - 1;

        return x3;
    }

    // установка позиции при скорлинге
    setPosition(x){
        this.abs.nativeElement.translateX += x - this.prevDeltaX;
        this.prevDeltaX = x;
    }

    
    // прогрутить до слайда
    moveToSlide(slide: number){
        let dep = slide * this.slideWidth; // длина до слайда
        let f = this.maxRight - dep; // высчитываем положение куда надо крутить
        this.toPosition(f)
        this.setActiveSlide(slide);
        // отправляем на верх активный фильтр
        this.currentFilter.emit(this.filters[slide])
    }

    // визуальная отметка активного слайда
    setActiveSlide(active: number){
        this.filters.forEach((filter: Filter, index) => {
            let wrap = this.abs.nativeElement.getChildAt(index); // обертка в виде StackLayout 
            let image: Image = wrap.getChildAt(0); // изображение
            let label = wrap.getChildAt(1); // текст
            // console.log(label.text, Math.random());
            // label.text = 'test' + Math.random();

            if(index == active){ // если слайд активный
                label.style.color = "black";
                
                image.style.opacity = 1;
                // label.className = "active";
            }else{
                label.style.color = "#959595";
                image.style.opacity = 0.4;
            }
            
        })
    }

    // прогрутить до позции
    toPosition(translateX){
        this.abs.nativeElement.animate({
            translate: { x: translateX, y: 0 },
            duration: this.duration,
            curve: AnimationCurve.easeOut
        })
    }

}
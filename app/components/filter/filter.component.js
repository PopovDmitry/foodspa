"use strict";
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var page_1 = require("ui/page");
var services_1 = require("../../services");
var absolute_layout_1 = require("ui/layouts/absolute-layout");
var stack_layout_1 = require("ui/layouts/stack-layout");
var label_1 = require("ui/label");
var image_1 = require("ui/image");
var gestures = require("ui/gestures");
var enums_1 = require("ui/enums");
var nativescript_web_image_cache_1 = require("nativescript-web-image-cache");
var FilterComponent = (function () {
    function FilterComponent(sharedService, router, page, imageService) {
        this.sharedService = sharedService;
        this.router = router;
        this.page = page;
        this.imageService = imageService;
        this.start = 0;
        this.currentFilter = new core_1.EventEmitter();
        this.slideWidth = this.sharedService.user.screen_width / 3;
        this.maxRight = this.slideWidth;
        this.duration = 200;
        this.currentSlide = 0;
        this.images = [];
        this.l = [];
    }
    FilterComponent.prototype.ngOnInit = function () {
        if (this.start > this.filters.length) {
            this.start = this.filters.length;
        }
        this.currentSlide = this.start;
    };
    FilterComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.wraperSlide.nativeElement.width = this.slideWidth * this.filters.length;
        var images = this.filters.map(function (v) { return v.image; });
        this.images = this.filters.map(function (v) { return '~/images/filterLoad.png'; });
        this.filters.forEach(function (filter, index) {
            var slide = new stack_layout_1.StackLayout();
            slide.width = _this.slideWidth;
            slide.style.textAlignment = "center";
            var image;
            if (_this.sharedService.isIOS) {
                image = new nativescript_web_image_cache_1.WebImage();
            }
            else {
                image = new image_1.Image();
            }
            image.src = filter.image;
            image.width = 100;
            image.height = 100;
            image.style.borderRadius = 50;
            image.style.marginBottom = 10;
            image.style.marginTop = image.style.marginBottom;
            var label = new label_1.Label();
            label.text = filter.title;
            label.style.marginBottom = 10;
            slide.addChild(image);
            slide.addChild(label);
            var position = index * _this.slideWidth;
            absolute_layout_1.AbsoluteLayout.setLeft(slide, position);
            _this.abs.nativeElement.addChild(slide);
        });
        this.abs.nativeElement.translateX = this.slideWidth;
        this.maxLeft = ((this.filters.length - 2) * this.slideWidth) * -1;
        this.moveToSlide(this.currentSlide);
        this.interaction();
    };
    FilterComponent.prototype.interaction = function () {
        var _this = this;
        this.wraperSlide.nativeElement.on('pan', function (args) {
            if (args.state === gestures.GestureStateTypes.began) {
                _this.prevDeltaX = 0;
            }
            else if (args.state === gestures.GestureStateTypes.ended) {
                if (_this.abs.nativeElement.translateX > _this.maxRight) {
                    _this.moveToSlide(0);
                    return;
                }
                if (_this.abs.nativeElement.translateX < _this.maxLeft) {
                    _this.moveToSlide(_this.filters.length - 1);
                    return;
                }
                _this.currentSlide = _this.translateXToSlide(_this.abs.nativeElement.translateX);
                _this.moveToSlide(_this.currentSlide);
            }
            else {
                _this.setPosition(args.deltaX);
                var active = _this.translateXToSlide(_this.abs.nativeElement.translateX);
                if (active != _this.currentSlide) {
                    _this.setActiveSlide(active);
                    active = null;
                }
            }
        });
    };
    FilterComponent.prototype.translateXToSlide = function (x) {
        if (x < this.maxLeft) {
            return this.filters.length - 1;
        }
        if (x > this.maxRight) {
            return 0;
        }
        var x1 = Math.round(this.maxLeft - x);
        if (x1 < 0) {
            x1 = x1 * -1;
        }
        var x2 = (this.filters.length * this.slideWidth) - x1;
        var x3 = Math.round(x2 / this.slideWidth) - 1;
        return x3;
    };
    FilterComponent.prototype.setPosition = function (x) {
        this.abs.nativeElement.translateX += x - this.prevDeltaX;
        this.prevDeltaX = x;
    };
    FilterComponent.prototype.moveToSlide = function (slide) {
        var dep = slide * this.slideWidth;
        var f = this.maxRight - dep;
        this.toPosition(f);
        this.setActiveSlide(slide);
        this.currentFilter.emit(this.filters[slide]);
    };
    FilterComponent.prototype.setActiveSlide = function (active) {
        var _this = this;
        this.filters.forEach(function (filter, index) {
            var wrap = _this.abs.nativeElement.getChildAt(index);
            var image = wrap.getChildAt(0);
            var label = wrap.getChildAt(1);
            if (index == active) {
                label.style.color = "black";
                image.style.opacity = 1;
            }
            else {
                label.style.color = "#959595";
                image.style.opacity = 0.4;
            }
        });
    };
    FilterComponent.prototype.toPosition = function (translateX) {
        this.abs.nativeElement.animate({
            translate: { x: translateX, y: 0 },
            duration: this.duration,
            curve: enums_1.AnimationCurve.easeOut
        });
    };
    return FilterComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Array)
], FilterComponent.prototype, "filters", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], FilterComponent.prototype, "start", void 0);
__decorate([
    core_1.ViewChild('filters'),
    __metadata("design:type", core_1.ElementRef)
], FilterComponent.prototype, "abs", void 0);
__decorate([
    core_1.ViewChild('wraperSlide'),
    __metadata("design:type", core_1.ElementRef)
], FilterComponent.prototype, "wraperSlide", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], FilterComponent.prototype, "currentFilter", void 0);
FilterComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'filter',
        templateUrl: 'filter.component.html',
    }),
    __metadata("design:paramtypes", [services_1.SharedService,
        router_1.RouterExtensions,
        page_1.Page,
        services_1.ImageService])
], FilterComponent);
exports.FilterComponent = FilterComponent;

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DatePicker } from "ui/date-picker";
import { TimePicker } from "ui/time-picker";
import { Page } from "ui/page";
import { SharedService } from "../../services";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";

@Component({
    moduleId: module.id,
    selector: 'datetime',
    templateUrl: 'datetime.component.html',
    styleUrls: ['datetime.css']
})

export class DateTimeComponent implements OnInit {

     @ViewChild("datePickerID") datePicker: ElementRef;
     @ViewChild("timePickerID") timePicker: ElementRef;
     private datePickerView;
     private timePickerView;
     private className: string;
    
    constructor(
        private params: ModalDialogParams, 
        private page: Page,
        private sharedService: SharedService
    ) { }


    ngOnInit() {
        this.page.actionBarHidden = true;  
        this.page.backgroundSpanUnderStatusBar = true;

        this.className = (this.sharedService.isIOS) ? 'ios' : 'android';

        let dat = new Date();

        // даты
        this.datePickerView = <DatePicker>this.datePicker.nativeElement;
        this.datePickerView.minDate = new Date();
        this.datePickerView.year = this.params.context.date.getFullYear();
        this.datePickerView.month = this.params.context.date.getMonth() + 1;
        this.datePickerView.day = this.params.context.date.getDate()
        this.datePickerView.maxDate = new Date(dat.setDate(dat.getDate() + 7));

        this.timePickerView = <TimePicker>this.timePicker.nativeElement;
        // this.timePickerView.minHour = 9;
        // this.timePickerView.maxHour = 21;
        this.timePickerView.minute = 0;
        this.timePickerView.minuteInterval = 10;

     }

     close(){
        let date = new Date(
            this.datePickerView.year, 
            this.datePickerView.month-1, 
            this.datePickerView.day,
            this.timePickerView.hour,
            this.timePickerView.minute,
            )

         this.params.closeCallback({result: date})
     } 
}
"use strict";
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var services_1 = require("../../services");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var DateTimeComponent = (function () {
    function DateTimeComponent(params, page, sharedService) {
        this.params = params;
        this.page = page;
        this.sharedService = sharedService;
    }
    DateTimeComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
        this.page.backgroundSpanUnderStatusBar = true;
        this.className = (this.sharedService.isIOS) ? 'ios' : 'android';
        var dat = new Date();
        this.datePickerView = this.datePicker.nativeElement;
        this.datePickerView.minDate = new Date();
        this.datePickerView.year = this.params.context.date.getFullYear();
        this.datePickerView.month = this.params.context.date.getMonth() + 1;
        this.datePickerView.day = this.params.context.date.getDate();
        this.datePickerView.maxDate = new Date(dat.setDate(dat.getDate() + 7));
        this.timePickerView = this.timePicker.nativeElement;
        this.timePickerView.minute = 0;
        this.timePickerView.minuteInterval = 10;
    };
    DateTimeComponent.prototype.close = function () {
        var date = new Date(this.datePickerView.year, this.datePickerView.month - 1, this.datePickerView.day, this.timePickerView.hour, this.timePickerView.minute);
        this.params.closeCallback({ result: date });
    };
    return DateTimeComponent;
}());
__decorate([
    core_1.ViewChild("datePickerID"),
    __metadata("design:type", core_1.ElementRef)
], DateTimeComponent.prototype, "datePicker", void 0);
__decorate([
    core_1.ViewChild("timePickerID"),
    __metadata("design:type", core_1.ElementRef)
], DateTimeComponent.prototype, "timePicker", void 0);
DateTimeComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'datetime',
        templateUrl: 'datetime.component.html',
        styleUrls: ['datetime.css']
    }),
    __metadata("design:paramtypes", [modal_dialog_1.ModalDialogParams,
        page_1.Page,
        services_1.SharedService])
], DateTimeComponent);
exports.DateTimeComponent = DateTimeComponent;

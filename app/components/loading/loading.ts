import { Component, OnInit} from "@angular/core";
import { Page } from "ui/page";
import { RouterExtensions } from "nativescript-angular/router";
import { ApiService, SharedService, ImageService } from "../../services";
import { Filter, Category, EventCacheModel } from '../../models';
import 'rxjs/add/operator/catch';
import {AnimationCurve} from "ui/enums";
var enums = require("ui/enums");
import platformModule = require("platform");
import { find } from "lodash";

import * as application from 'application';
let TnsOneSignal = require('nativescript-onesignal').TnsOneSignal;


// import fpsMeter = require("fps-meter");


@Component({
    selector: "loading",
    moduleId: module.id,
    templateUrl: "./loading.html",
    styleUrls: ['./loading.css'],
})

export class LoadingComponent implements OnInit {

    private loadProcent: number = 0;
    private logo; // контенер с логотипом
    private text; // контейнер с текстом
    private loader; // контенер с индикором загрузки

    constructor(
        private page: Page,
        private router: RouterExtensions,
        private apiService: ApiService,
        private sharedService: SharedService,
        private imageService: ImageService,
        ){
    }

    ngOnInit(){
        // прячем actionBAR для андроида и на всякий для IOS
        this.page.actionBarHidden = true;  
        this.page.backgroundSpanUnderStatusBar = true;

        this.sharedService.initObservable(); 

        
        
        // var callbackId = fpsMeter.addCallback(function (fps: number, minFps: number) {
        //     console.info("fps=" + fps + " minFps=" + minFps);
        // });
        // fpsMeter.start();
        // fpsMeter.removeCallback(callbackId);
        // fpsMeter.stop();

        // if(this.sharedService.isIOS){
        //     TnsOneSignal.IdsAvailable(function(userId, registrationId) {
        //         if(userId != 'nil' || userId != "" || userId != undefined) {
        //             console.log("+++++++++++++++++++++APP ID", TnsOneSignal.app_id());
        //             console.log("+++++++++++++++++++++trying to get IDS...", userId);
        //             console.log("+++++++++++++++++++++REG...", registrationId);
                    
        //             TnsOneSignal.registerForPushNotifications();
        //             // TnsOneSignal.setSubscription(true);
        //             console.log('GOOD');
        //         }else{
        //             console.log("+++++++++++++++++++++ERROR trying to get IDS...");
        //         }
            
        //     });
        // }

        


        // OneSignal.IdsAvailable(function (id) {
        //     console.log('id', id)
        // })
        

        this.sharedService.api = this.apiService;
        // console.profile()
        this.logo = this.page.getViewById("logo")
        this.text = this.page.getViewById("text")
        this.loader = this.page.getViewById("loader")
        
        
        // console.profileEnd();
        // console.debug('restTest', {rest: 1})

        // console.dump({rest2: 1})
        // console.log('log',{rest2: 1})

        // console.log('staffer');
        
        

        // вызов начальной анимации
        setTimeout(() => {
            this.ainmationIn().then(() => {
                this.issetDivice() // проверяем устройство
                this.all(); // начинаем загрузку данных
                this.loger(); // выводим лог
                
                
            }); 
        }, 400)
    }

    // выводим логе рособытий
    loger(){
        this.sharedService
            .observable
            .subscribe( v => {
                console.log('EVENT ++++++++++++++', v.type);        
            })
    }

    // иницализируем push
    pushInits(){
        if (application.ios) {
            class Delegate extends UIResponder implements UIApplicationDelegate {
                public static ObjCProtocols = [UIApplicationDelegate]
                applicationDidFinishLaunchingWithOptions(nativeApp: UIApplication, launchOptions: NSDictionary<any, any>): boolean {
                    try {
                        TnsOneSignal.initWithLaunchOptionsAppId(launchOptions, 'dfefb395-459a-40a3-bbc6-1c00c727dd26')
                    } catch (error) {
                        console.error('error', error)
                    }
                    return true
                }
            }
            application.ios.delegate = Delegate
        }


        if (application.android) {
            application.on(application.launchEvent, function(args: application.ApplicationEventData) {
                try {
                    TnsOneSignal.startInit(application.android.context).init()
                } catch (error) {
                    console.error('error', error)
                }
            })
        }
    }

    // грузим все данные
    all(){
        this.sharedService
            .observable
            .filter( (v) => v.type == this.sharedService.eventTypes.api.start)
            .subscribe( v => {
                this.apiService.loadAll().subscribe(data => {
                    
                    // наполняем основной каталог с данными
                    this.sharedService.data = data.Category;

                    // наполняем корзину
                    if(data.Order != null){
                        this.sharedService.basket = data.Order; 
                    }

                    // наполняем контакты
                    this.sharedService.contacts = data.Contacts;

                    // наполняем адреса
                    this.sharedService.addresses = data.Addresses;

                    // кешиируем получаем все список изображений
                    let images: string[] = this.sharedService.data.map( v => v.image)

                    // начинаем первоначальную загрузку изображений
                    this.imageService.loadAll(images).then((res: EventCacheModel[]) => {
                        this.sharedService.data.forEach((cat: Category, i) => {
                            let image = find(res, (z: EventCacheModel) => z.url == cat.image);
                            if(image){
                                cat.image = image.source;
                            }
                        })

                        // завершаем обработку через 3 секунды
                        setTimeout(()=> {
                            this.animationOut().then(() => {
                                this._redirect('index')
                                // this._redirect('activation/push') 
                                // this._redirect('index/category/detox/view/649')
                                // this._redirect('basket')
                                // this._redirect('index/category/detox')
                                // this._redirect('basket/two')
                                // this._redirect('basket/three')    
                            })
                        }, 3000)
                    })                    
                }, error => {

                })
            })
    }

    // проверка на существовании девайса
    issetDivice(){
        this.apiService.issetDevice(this.sharedService.uuid)
            .subscribe( v => {
                this.sharedService.user = v;
                this.sharedService.sendNext(this.sharedService.eventTypes.api.user, v);
                if(v.app_valid_sms == false){
                    // нет активации по смс
                    this._redirect('activation')
                }else{
                    if(this.sharedService.user.push){
                        this.pushInits();
                    }
                    this.sharedService.sendNext(this.sharedService.eventTypes.api.start, true)
                }
            }, onerror => {
                onerror.uuid = this.sharedService.uuid;
                this.sharedService.sendNext(this.sharedService.eventTypes.error.http, onerror)
                if(onerror.msg == "Device does not exist"){
                    this._redirect('activation')
                }
            })
    }

    // анимация начало загрузки 
    ainmationIn(){
        return new Promise((resolve, reject) =>{
            this.logo.animate({
                translate: { x: 0, y: -3 },
                opacity: 1,
                duration: 1000,
                curve: AnimationCurve.easeInOut
            }).then(()=>{});

            setTimeout(() => {
                this.text.animate({
                    translate: { x: 0, y: -12 },
                    opacity: 1,
                    duration: 1000,
                    curve: AnimationCurve.easeInOut
                }).then(()=>{});
            }, 200)

            setTimeout(() => {
                this.loader.animate({
                    opacity: 1,
                    duration: 500,
                    curve: AnimationCurve.easeInOut
                }).then(()=>{
                    resolve()
                });
            }, 1000)
        })
        
    }

    // анимация конца загрузки
    animationOut(){
        return new Promise((resolve, reject) =>{
            // this.loader.animate({
            //         opacity: 0,
            //         duration: 500,
            //         curve: AnimationCurve.easeInOut
            //     }).then(()=>{});

            setTimeout(() => {
                this.text.animate({
                    translate: { x: 0, y: 0 },
                    opacity: 0,
                    duration: 1000,
                    curve: AnimationCurve.easeInOut
                }).then(()=>{});
            }, 200)

            setTimeout(() => {

                this.logo.animate({
                        translate: { x: 0, y: 0 },
                        opacity: 0,
                        duration: 1000,
                        curve: AnimationCurve.easeInOut
                    }).then(()=>{resolve()});
            }, 500)
        })
        
    }

    _redirect(url: string){
        
        this.sharedService.sendNext(this.sharedService.eventTypes.actions.redirect, {url: url});
        this.router.navigate([url], {
                        clearHistory: true,
                        transition: {
                            name: 'fade',
                            // curve: 'easeOut',
                            // duration: 500
                        }
                    });   
    }
}
"use strict";
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var router_1 = require("nativescript-angular/router");
var services_1 = require("../../services");
require("rxjs/add/operator/catch");
var enums_1 = require("ui/enums");
var enums = require("ui/enums");
var lodash_1 = require("lodash");
var application = require("application");
var TnsOneSignal = require('nativescript-onesignal').TnsOneSignal;
var LoadingComponent = (function () {
    function LoadingComponent(page, router, apiService, sharedService, imageService) {
        this.page = page;
        this.router = router;
        this.apiService = apiService;
        this.sharedService = sharedService;
        this.imageService = imageService;
        this.loadProcent = 0;
    }
    LoadingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.page.actionBarHidden = true;
        this.page.backgroundSpanUnderStatusBar = true;
        this.sharedService.initObservable();
        this.sharedService.api = this.apiService;
        this.logo = this.page.getViewById("logo");
        this.text = this.page.getViewById("text");
        this.loader = this.page.getViewById("loader");
        setTimeout(function () {
            _this.ainmationIn().then(function () {
                _this.issetDivice();
                _this.all();
                _this.loger();
            });
        }, 400);
    };
    LoadingComponent.prototype.loger = function () {
        this.sharedService
            .observable
            .subscribe(function (v) {
            console.log('EVENT ++++++++++++++', v.type);
        });
    };
    LoadingComponent.prototype.pushInits = function () {
        if (application.ios) {
            var Delegate = (function (_super) {
                __extends(Delegate, _super);
                function Delegate() {
                    return _super !== null && _super.apply(this, arguments) || this;
                }
                Delegate.prototype.applicationDidFinishLaunchingWithOptions = function (nativeApp, launchOptions) {
                    try {
                        TnsOneSignal.initWithLaunchOptionsAppId(launchOptions, 'dfefb395-459a-40a3-bbc6-1c00c727dd26');
                    }
                    catch (error) {
                        console.error('error', error);
                    }
                    return true;
                };
                return Delegate;
            }(UIResponder));
            Delegate.ObjCProtocols = [UIApplicationDelegate];
            application.ios.delegate = Delegate;
        }
        if (application.android) {
            application.on(application.launchEvent, function (args) {
                try {
                    TnsOneSignal.startInit(application.android.context).init();
                }
                catch (error) {
                    console.error('error', error);
                }
            });
        }
    };
    LoadingComponent.prototype.all = function () {
        var _this = this;
        this.sharedService
            .observable
            .filter(function (v) { return v.type == _this.sharedService.eventTypes.api.start; })
            .subscribe(function (v) {
            _this.apiService.loadAll().subscribe(function (data) {
                _this.sharedService.data = data.Category;
                if (data.Order != null) {
                    _this.sharedService.basket = data.Order;
                }
                _this.sharedService.contacts = data.Contacts;
                _this.sharedService.addresses = data.Addresses;
                var images = _this.sharedService.data.map(function (v) { return v.image; });
                _this.imageService.loadAll(images).then(function (res) {
                    _this.sharedService.data.forEach(function (cat, i) {
                        var image = lodash_1.find(res, function (z) { return z.url == cat.image; });
                        if (image) {
                            cat.image = image.source;
                        }
                    });
                    setTimeout(function () {
                        _this.animationOut().then(function () {
                            _this._redirect('index');
                        });
                    }, 3000);
                });
            }, function (error) {
            });
        });
    };
    LoadingComponent.prototype.issetDivice = function () {
        var _this = this;
        this.apiService.issetDevice(this.sharedService.uuid)
            .subscribe(function (v) {
            _this.sharedService.user = v;
            _this.sharedService.sendNext(_this.sharedService.eventTypes.api.user, v);
            if (v.app_valid_sms == false) {
                _this._redirect('activation');
            }
            else {
                if (_this.sharedService.user.push) {
                    _this.pushInits();
                }
                _this.sharedService.sendNext(_this.sharedService.eventTypes.api.start, true);
            }
        }, function (onerror) {
            onerror.uuid = _this.sharedService.uuid;
            _this.sharedService.sendNext(_this.sharedService.eventTypes.error.http, onerror);
            if (onerror.msg == "Device does not exist") {
                _this._redirect('activation');
            }
        });
    };
    LoadingComponent.prototype.ainmationIn = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.logo.animate({
                translate: { x: 0, y: -3 },
                opacity: 1,
                duration: 1000,
                curve: enums_1.AnimationCurve.easeInOut
            }).then(function () { });
            setTimeout(function () {
                _this.text.animate({
                    translate: { x: 0, y: -12 },
                    opacity: 1,
                    duration: 1000,
                    curve: enums_1.AnimationCurve.easeInOut
                }).then(function () { });
            }, 200);
            setTimeout(function () {
                _this.loader.animate({
                    opacity: 1,
                    duration: 500,
                    curve: enums_1.AnimationCurve.easeInOut
                }).then(function () {
                    resolve();
                });
            }, 1000);
        });
    };
    LoadingComponent.prototype.animationOut = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            setTimeout(function () {
                _this.text.animate({
                    translate: { x: 0, y: 0 },
                    opacity: 0,
                    duration: 1000,
                    curve: enums_1.AnimationCurve.easeInOut
                }).then(function () { });
            }, 200);
            setTimeout(function () {
                _this.logo.animate({
                    translate: { x: 0, y: 0 },
                    opacity: 0,
                    duration: 1000,
                    curve: enums_1.AnimationCurve.easeInOut
                }).then(function () { resolve(); });
            }, 500);
        });
    };
    LoadingComponent.prototype._redirect = function (url) {
        this.sharedService.sendNext(this.sharedService.eventTypes.actions.redirect, { url: url });
        this.router.navigate([url], {
            clearHistory: true,
            transition: {
                name: 'fade',
            }
        });
    };
    return LoadingComponent;
}());
LoadingComponent = __decorate([
    core_1.Component({
        selector: "loading",
        moduleId: module.id,
        templateUrl: "./loading.html",
        styleUrls: ['./loading.css'],
    }),
    __metadata("design:paramtypes", [page_1.Page,
        router_1.RouterExtensions,
        services_1.ApiService,
        services_1.SharedService,
        services_1.ImageService])
], LoadingComponent);
exports.LoadingComponent = LoadingComponent;

"use strict";
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var services_1 = require("../../services");
var router_1 = require("nativescript-angular/router");
var dialogs = require("ui/dialogs");
var GetCodeComponent = (function () {
    function GetCodeComponent(page, router, apiService, sharedService) {
        this.page = page;
        this.router = router;
        this.apiService = apiService;
        this.sharedService = sharedService;
        this.user = { app_phone: '' };
        this.isLoading = false;
        this.timerDefault = 90;
    }
    GetCodeComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
        this.page.backgroundSpanUnderStatusBar = true;
        this.timer = this.timerDefault;
        this.startTimer();
    };
    GetCodeComponent.prototype.startTimer = function () {
        var _this = this;
        if (this.timer > 0) {
            if (this.timerIndifer != undefined) {
                clearTimeout(this.timerIndifer);
            }
            this.timer--;
            this.timerIndifer = setTimeout(function () {
                _this.startTimer();
            }, 1000);
        }
        else if (this.timer == 0) {
            if (this.timerIndifer != undefined) {
                clearTimeout(this.timerIndifer);
            }
        }
    };
    GetCodeComponent.prototype.resendCode = function () {
        this.sharedService.api
            .resendCode(this.sharedService.user.app_phone)
            .subscribe(function (res) {
        });
        this.timer = this.timerDefault;
        this.startTimer();
    };
    GetCodeComponent.prototype.onFinish = function () {
        var _this = this;
        var code = this.code.nativeElement.text.replace(/[^0-9.]/g, "");
        if (code.length != 4) {
            this.showAlert('Введите пожалуста код 4х значный код');
        }
        else {
            this.apiService.checkCode(code).subscribe(function (v) {
                _this.sharedService.sendNext(_this.sharedService.eventTypes.actions.redirect, true);
                var url = (_this.sharedService.isIOS) ? "/activation/push" : "/loading";
                _this.router.navigate([url], {
                    clearHistory: true,
                }).then(function (s) {
                    _this.isLoading = false;
                });
            }, function (error) {
                if (error.msg == "Incorrect code") {
                    _this.showAlert('Код не верен');
                }
            });
        }
    };
    GetCodeComponent.prototype.showAlert = function (msg, callback) {
        if (callback === void 0) { callback = function () { }; }
        dialogs.alert(msg).then(callback);
    };
    return GetCodeComponent;
}());
__decorate([
    core_1.ViewChild("code"),
    __metadata("design:type", core_1.ElementRef)
], GetCodeComponent.prototype, "code", void 0);
GetCodeComponent = __decorate([
    core_1.Component({
        selector: 'get-code',
        moduleId: module.id,
        templateUrl: './get-code.component.html',
        styleUrls: ['./get-code.css'],
    }),
    __metadata("design:paramtypes", [page_1.Page,
        router_1.RouterExtensions,
        services_1.ApiService,
        services_1.SharedService])
], GetCodeComponent);
exports.GetCodeComponent = GetCodeComponent;

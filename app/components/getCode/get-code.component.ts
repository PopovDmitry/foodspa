import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { Page } from "ui/page";
import { Router, NavigationExtras } from "@angular/router";
import { ApiService, SharedService } from "../../services";
import { RouterExtensions } from "nativescript-angular/router";
import dialogs = require("ui/dialogs");


@Component({
    selector: 'get-code',
    moduleId: module.id,
    templateUrl: './get-code.component.html',
    styleUrls: ['./get-code.css'],
    // providers: [ApiService]
})

export class GetCodeComponent implements OnInit{

    public user: any = {app_phone: ''};
    private isLoading: boolean = false;
    private timerDefault: number = 90;
    private timer: number;
    private timerIndifer: any;

    @ViewChild("code") code: ElementRef;
    

    constructor(
        private page: Page,  
        private router: RouterExtensions, 
        private apiService: ApiService,
        private sharedService: SharedService
        ){
    }

    ngOnInit(): void{
        this.page.actionBarHidden = true;
        this.page.backgroundSpanUnderStatusBar = true;
        this.timer = this.timerDefault;
        this.startTimer();
        // this.user = this.sharedService.user
    }

    startTimer(){
        if(this.timer > 0){
            if(this.timerIndifer != undefined){
                clearTimeout(this.timerIndifer);
            }

            this.timer--;

            this.timerIndifer = setTimeout(() => {
                this.startTimer()
            }, 1000)
        }else if(this.timer == 0){
            if(this.timerIndifer != undefined){
                clearTimeout(this.timerIndifer);
            }
        }
    }

    // повторная отраавка кода активации
    resendCode(){
        this.sharedService.api
            .resendCode(this.sharedService.user.app_phone)
            .subscribe(res => {
                
            });
        this.timer = this.timerDefault;
        this.startTimer();
    }

    onFinish(){
        // console.dump(this.apiService.test)
        
        var code = this.code.nativeElement.text.replace(/[^0-9.]/g, "");
        if(code.length != 4){
            this.showAlert('Введите пожалуста код 4х значный код')
        }else{
            // проверка введеного кода
            this.apiService.checkCode(code).subscribe(v => {
                    this.sharedService.sendNext(this.sharedService.eventTypes.actions.redirect, true);

                    let url = (this.sharedService.isIOS) ? "/activation/push" : "/loading";

                    this.router.navigate([url], {
                        clearHistory: true, 
                    }).then( s => {
                        this.isLoading = false;
                    })
                },
                error => {
                    if(error.msg == "Incorrect code"){
                        this.showAlert('Код не верен')    
                    }
                }
            )
            // if(this.sharedService.user.app_sms_code == code){
            //     this.isLoading = true;
            //     this.sharedService.user.app_valid_sms = true;
            //     // this.apiService.setValueByUser('/users', this.sharedService.user).then(success => {
            //         this.sharedService.sendNext(this.sharedService.eventTypes.actions.redirect, true);
            //         this.router.navigate(["/index"], {
            //             clearHistory: true, 
            //         }).then( s => {
            //             this.isLoading = false;
            //         })
            //     // })
            // }else{
            //     this.showAlert('Код не верен')    
            // }
        }

        
    }

    // выводит алерт и по необходисости вызывает функцию
    showAlert(msg: string, callback = ()=>{}){
        dialogs.alert(msg).then(callback);
    }
}
import { Component, OnInit, Input } from '@angular/core';
import { Page } from "ui/page";
import {AnimationCurve} from "ui/enums";

@Component({
    moduleId: module.id,
    selector: 'index-item',
    templateUrl: 'index-item.component.html',
    styleUrls: ['index-item.css']
})
export class IndexItemComponent implements OnInit {
    @Input() data: any // данные
    @Input() index: number // index
    private delayStep: number = 200; // через какое колличество наичнаем показ
    private duration: number = 700; // время анимации
    private delayStart: number = 500;

    constructor(
        private page: Page,
    ) { }

    ngOnInit() {
        
     }

     ngAfterViewInit(){
        setTimeout(()=>{
            this.animationIn();
        }, (this.delayStep * this.index) + this.delayStart)
        
     }

     // показать анимацию
     animationIn(){
        // начинаем через сколько начать показывать
        this.page.getViewById('index' + this.index).animate({
            translate: { x: -20, y: 0 },
            opacity: 1,
            duration: this.duration,
            curve: AnimationCurve.easeOut
        }).then(()=>{
            // конец анимации
        })
     }

     
}
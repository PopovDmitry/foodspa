"use strict";
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var enums_1 = require("ui/enums");
var IndexItemComponent = (function () {
    function IndexItemComponent(page) {
        this.page = page;
        this.delayStep = 200;
        this.duration = 700;
        this.delayStart = 500;
    }
    IndexItemComponent.prototype.ngOnInit = function () {
    };
    IndexItemComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () {
            _this.animationIn();
        }, (this.delayStep * this.index) + this.delayStart);
    };
    IndexItemComponent.prototype.animationIn = function () {
        this.page.getViewById('index' + this.index).animate({
            translate: { x: -20, y: 0 },
            opacity: 1,
            duration: this.duration,
            curve: enums_1.AnimationCurve.easeOut
        }).then(function () {
        });
    };
    return IndexItemComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], IndexItemComponent.prototype, "data", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], IndexItemComponent.prototype, "index", void 0);
IndexItemComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'index-item',
        templateUrl: 'index-item.component.html',
        styleUrls: ['index-item.css']
    }),
    __metadata("design:paramtypes", [page_1.Page])
], IndexItemComponent);
exports.IndexItemComponent = IndexItemComponent;

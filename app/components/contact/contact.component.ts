import { Component, OnInit, NgModule, ElementRef, ViewChild } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { DatePicker } from "ui/date-picker";
import { Page } from "ui/page";
import { Contact } from '../../models/';
import { SharedService } from "../../services";
import label = require("ui/label");

@Component({
    moduleId: module.id,
    selector: 'contact',
    templateUrl: 'contact.component.html',
    styleUrls: ['contact.css']
})
export class ContactComponent implements OnInit {
    
    private isSubmit = false;
    
    @ViewChild("fio") fio: ElementRef;
    @ViewChild("phone") phone: ElementRef; 
    @ViewChild("email") email: ElementRef;
    // @ViewChild("name") name: ElementRef;

    private scroll: number;
    private errorDisplay: boolean = false;
    private errorMSG: string = '';

    constructor(
        private params: ModalDialogParams, 
        private page: Page,
        private sharedService: SharedService
    ) { }

    ngOnInit() { 
        this.page.actionBarHidden = true;  
        this.page.backgroundSpanUnderStatusBar = true;

        if(this.sharedService.isAndroid){
            this.scroll = this.sharedService.user.screen_height / 2
        }else{
            this.scroll = this.sharedService.user.screen_height
        }

        if(this.params.context.type == 'add'){
            this.fio.nativeElement.text = '';
            this.phone.nativeElement.text = (this.sharedService.contacts.length == 0) ? this.sharedService.user.app_phone : '' ; // при самом первом запуске дополняем данные
            this.email.nativeElement.text = ''
            // this.name.nativeElement.text = Math.random()
        }else{
            //console.dump(this.params.context.data)
            this.fio.nativeElement.text = this.params.context.data.fio 
            this.phone.nativeElement.text = this.params.context.data.phone 
            this.email.nativeElement.text = this.params.context.data.email
            // this.name.nativeElement.text = this.params.context.data.name 
            // this.name.nativeElement.text = Math.random()
        }
    }

    // закрыть
     close(){
         this.params.closeCallback(0)
     } 

     // обновить данные
     save(){
        let contact: Contact;
        contact = new Contact(
            this.params.context.data.id, 
            this.fio.nativeElement.text,
            this.phone.nativeElement.text,
            this.email.nativeElement.text,
            Math.random().toString(),
        )

        if(contact.fio == ""){
            this.toogleError(true, 'Укажите ваше имя')
            this.setBorderColor('fio', 'red')
        }else if(contact.email == ""){
            this.toogleError(true, 'Введите E-mail')
            this.setBorderColor('email', 'red')
        }else if(!contact.phone){
            this.toogleError(true, 'Введите телефон')
            this.setBorderColor('phone', 'red')
        }else{
            if(!this.validateEmail(contact.email)){
                this.errorMSG = 'не корректный email';
                this.toogleError(true, this.errorMSG)
                this.setBorderColor('email', 'red')
            }else{
                this.toogleError(false)
                this.setBorderColor('email')
                this.isSubmit = true;
                this.sharedService.api.updateContact(contact)
                    .subscribe(v => {
                        this.sharedService.contacts[this.params.context.index] = contact;
                        setTimeout(() => {
                            this.isSubmit = false;
                            this.params.closeCallback({result: true})
                        }, 2000)
                    }, error => {
                        // console.dump(error)
                        this.params.closeCallback({result: false})
                    })
            }
        }

        
     }

     // добавление контакта
     add(){
        let contact: Contact;
        contact = new Contact(
            0,
            this.fio.nativeElement.text,
            this.phone.nativeElement.text,
            this.email.nativeElement.text,
            Math.random().toString(),
        )

        if(contact.fio == ""){
            this.toogleError(true, 'Укажите ваше имя')
            this.setBorderColor('fio', 'red')
        }else if(contact.email == ""){
            this.toogleError(true, 'Введите E-mail')
            this.setBorderColor('email', 'red')
        }else if(!contact.phone){
            this.toogleError(true, 'Введите телефон')
            this.setBorderColor('phone', 'red')
        }else{
            
            // console.log('emailTEST', this.validateEmail(contact.email));
            
            if(!this.validateEmail(contact.email)){
                this.errorMSG = 'не корректный email';
                this.toogleError(true, this.errorMSG)
                this.setBorderColor('email', 'red')
            }else{
                this.toogleError(false)
                this.setBorderColor('email')
                this.sharedService.api.createContact(contact)
                .subscribe(v => {
                    this.isSubmit = true;
                    contact.id = v.id
                    this.sharedService.contacts.push(contact)
                    setTimeout(() => {
                        this.isSubmit = false;
                        this.params.closeCallback({result: true})
                    }, 2000)
                    // this.params.closeCallback({result: true})
                }, error => {
                    // console.dump(error)
                    this.params.closeCallback({result: false})
                })
            }

        }

        
     }

    //  toogleError(display: boolean = false, msg: string = "Все поля должны быть заполнены"){

    //      let view = this.page.getViewById('errorDisplay')
    //      this.errorMSG = msg;
    //      view.animate({
    //          opacity: (display) ? 1 : 0,
    //          duration: 1000
    //      }).then(() => {

    //      })
    //  }

    setBorderColor(el: string, color: string = '#5871b6'){
        this.fio.nativeElement.borderColor = '#5871b6'
        this.phone.nativeElement.borderColor = '#5871b6'
        this.email.nativeElement.borderColor = '#5871b6'
        if(this[el]){
            this[el].nativeElement.borderColor = color;
        }  
     }

    toogleError(display: boolean = false, msg: string = "Ошибка сохранения"){
         if(display){
            this.errorDisplay = display;
            setTimeout(()=> {
                let view = <label.Label>this.page.getViewById('errorDisplay')
                view.text = msg;
                view.animate({
                    opacity: (display) ? 1 : 0,
                    duration: 1000
                }).then(() => {

                })    
            }, 200)
         }else{
            let view = this.page.getViewById('errorDisplay')
            if(view != undefined){
                view.animate({
                    opacity: (display) ? 1 : 0,
                    duration: 1000
                }).then(() => {
                    this.errorDisplay = display;
                })
            }
         }
     }

    validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

}
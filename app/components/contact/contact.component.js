"use strict";
var core_1 = require("@angular/core");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var page_1 = require("ui/page");
var _1 = require("../../models/");
var services_1 = require("../../services");
var ContactComponent = (function () {
    function ContactComponent(params, page, sharedService) {
        this.params = params;
        this.page = page;
        this.sharedService = sharedService;
        this.isSubmit = false;
        this.errorDisplay = false;
        this.errorMSG = '';
    }
    ContactComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
        this.page.backgroundSpanUnderStatusBar = true;
        if (this.sharedService.isAndroid) {
            this.scroll = this.sharedService.user.screen_height / 2;
        }
        else {
            this.scroll = this.sharedService.user.screen_height;
        }
        if (this.params.context.type == 'add') {
            this.fio.nativeElement.text = '';
            this.phone.nativeElement.text = (this.sharedService.contacts.length == 0) ? this.sharedService.user.app_phone : '';
            this.email.nativeElement.text = '';
        }
        else {
            this.fio.nativeElement.text = this.params.context.data.fio;
            this.phone.nativeElement.text = this.params.context.data.phone;
            this.email.nativeElement.text = this.params.context.data.email;
        }
    };
    ContactComponent.prototype.close = function () {
        this.params.closeCallback(0);
    };
    ContactComponent.prototype.save = function () {
        var _this = this;
        var contact;
        contact = new _1.Contact(this.params.context.data.id, this.fio.nativeElement.text, this.phone.nativeElement.text, this.email.nativeElement.text, Math.random().toString());
        if (contact.fio == "") {
            this.toogleError(true, 'Укажите ваше имя');
            this.setBorderColor('fio', 'red');
        }
        else if (contact.email == "") {
            this.toogleError(true, 'Введите E-mail');
            this.setBorderColor('email', 'red');
        }
        else if (!contact.phone) {
            this.toogleError(true, 'Введите телефон');
            this.setBorderColor('phone', 'red');
        }
        else {
            if (!this.validateEmail(contact.email)) {
                this.errorMSG = 'не корректный email';
                this.toogleError(true, this.errorMSG);
                this.setBorderColor('email', 'red');
            }
            else {
                this.toogleError(false);
                this.setBorderColor('email');
                this.isSubmit = true;
                this.sharedService.api.updateContact(contact)
                    .subscribe(function (v) {
                    _this.sharedService.contacts[_this.params.context.index] = contact;
                    setTimeout(function () {
                        _this.isSubmit = false;
                        _this.params.closeCallback({ result: true });
                    }, 2000);
                }, function (error) {
                    _this.params.closeCallback({ result: false });
                });
            }
        }
    };
    ContactComponent.prototype.add = function () {
        var _this = this;
        var contact;
        contact = new _1.Contact(0, this.fio.nativeElement.text, this.phone.nativeElement.text, this.email.nativeElement.text, Math.random().toString());
        if (contact.fio == "") {
            this.toogleError(true, 'Укажите ваше имя');
            this.setBorderColor('fio', 'red');
        }
        else if (contact.email == "") {
            this.toogleError(true, 'Введите E-mail');
            this.setBorderColor('email', 'red');
        }
        else if (!contact.phone) {
            this.toogleError(true, 'Введите телефон');
            this.setBorderColor('phone', 'red');
        }
        else {
            if (!this.validateEmail(contact.email)) {
                this.errorMSG = 'не корректный email';
                this.toogleError(true, this.errorMSG);
                this.setBorderColor('email', 'red');
            }
            else {
                this.toogleError(false);
                this.setBorderColor('email');
                this.sharedService.api.createContact(contact)
                    .subscribe(function (v) {
                    _this.isSubmit = true;
                    contact.id = v.id;
                    _this.sharedService.contacts.push(contact);
                    setTimeout(function () {
                        _this.isSubmit = false;
                        _this.params.closeCallback({ result: true });
                    }, 2000);
                }, function (error) {
                    _this.params.closeCallback({ result: false });
                });
            }
        }
    };
    ContactComponent.prototype.setBorderColor = function (el, color) {
        if (color === void 0) { color = '#5871b6'; }
        this.fio.nativeElement.borderColor = '#5871b6';
        this.phone.nativeElement.borderColor = '#5871b6';
        this.email.nativeElement.borderColor = '#5871b6';
        if (this[el]) {
            this[el].nativeElement.borderColor = color;
        }
    };
    ContactComponent.prototype.toogleError = function (display, msg) {
        var _this = this;
        if (display === void 0) { display = false; }
        if (msg === void 0) { msg = "Ошибка сохранения"; }
        if (display) {
            this.errorDisplay = display;
            setTimeout(function () {
                var view = _this.page.getViewById('errorDisplay');
                view.text = msg;
                view.animate({
                    opacity: (display) ? 1 : 0,
                    duration: 1000
                }).then(function () {
                });
            }, 200);
        }
        else {
            var view = this.page.getViewById('errorDisplay');
            if (view != undefined) {
                view.animate({
                    opacity: (display) ? 1 : 0,
                    duration: 1000
                }).then(function () {
                    _this.errorDisplay = display;
                });
            }
        }
    };
    ContactComponent.prototype.validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
    return ContactComponent;
}());
__decorate([
    core_1.ViewChild("fio"),
    __metadata("design:type", core_1.ElementRef)
], ContactComponent.prototype, "fio", void 0);
__decorate([
    core_1.ViewChild("phone"),
    __metadata("design:type", core_1.ElementRef)
], ContactComponent.prototype, "phone", void 0);
__decorate([
    core_1.ViewChild("email"),
    __metadata("design:type", core_1.ElementRef)
], ContactComponent.prototype, "email", void 0);
ContactComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'contact',
        templateUrl: 'contact.component.html',
        styleUrls: ['contact.css']
    }),
    __metadata("design:paramtypes", [modal_dialog_1.ModalDialogParams,
        page_1.Page,
        services_1.SharedService])
], ContactComponent);
exports.ContactComponent = ContactComponent;

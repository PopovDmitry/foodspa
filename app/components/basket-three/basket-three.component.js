"use strict";
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var router_1 = require("nativescript-angular/router");
var services_1 = require("../../services");
var models_1 = require("../../models");
var lodash_1 = require("lodash");
var BasketThreeComponent = (function () {
    function BasketThreeComponent(sharedService, page, router) {
        this.sharedService = sharedService;
        this.page = page;
        this.router = router;
        this.listViewHeight = 0;
        this.products = [];
        this.allSum = 0;
        this.allSumText = "";
    }
    BasketThreeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.page.actionBarHidden = true;
        this.page.backgroundSpanUnderStatusBar = true;
        if (this.sharedService.basket.address_id != 0) {
            var findAddress = lodash_1.find(this.sharedService.addresses, function (v) { return v.id == _this.sharedService.basket.address_id; });
            this.address = findAddress.city + ", " + findAddress.street + ", " + findAddress.house + ", " + findAddress.room;
        }
        else {
            this.address = "\u0421\u0430\u043C\u043E\u0432\u044B\u0432\u043E\u0437";
        }
        this.date = this.sharedService.basket.date_time.date + ", " + this.sharedService.basket.date_time.time;
        this.basketInfoToProducts();
        this.listViewHeight = this.sharedService.user.screen_height - 30;
        this.class = (this.sharedService.isIOS) ? 'ios' : 'android';
    };
    BasketThreeComponent.prototype.finish = function () {
        var _this = this;
        this.sharedService.basket.status = 'finish';
        this.sharedService.saveBasket().then(function (success) {
            _this.router.navigate(["index"], {});
            _this.sharedService.basket = new models_1.Basket();
        });
    };
    BasketThreeComponent.prototype.basketInfoToProducts = function () {
        var _this = this;
        var products = [];
        this.allSum = 0;
        this.sharedService.basket.products.forEach(function (v) {
            var pr = _this.getProductById(v.product_id);
            if (pr != null) {
                _this.allSum += v.product_count * pr.price;
            }
        });
        this.allSumText = "\u0421\u0443\u043C\u043C\u0430 \u0437\u0430\u043A\u0430\u0437\u0430: " + this.allSum + " \u0440\u0443\u0431.";
        return this.products;
    };
    BasketThreeComponent.prototype.getProductById = function (id) {
        return this.sharedService.data.reduce(function (result, value, key) {
            var res = lodash_1.find(value['products'], function (v) { return v.pid == id; });
            if (res != null) {
                result = res;
            }
            return result;
        }, {});
    };
    return BasketThreeComponent;
}());
BasketThreeComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'basket-three',
        templateUrl: 'basket-three.component.html',
        styleUrls: ['basket-three.css']
    }),
    __metadata("design:paramtypes", [services_1.SharedService,
        page_1.Page,
        router_1.RouterExtensions])
], BasketThreeComponent);
exports.BasketThreeComponent = BasketThreeComponent;

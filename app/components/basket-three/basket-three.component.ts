import { Component, OnInit } from '@angular/core';
import { Page } from "ui/page";
import { RouterExtensions } from "nativescript-angular/router";
import { SharedService } from '../../services'
import { Basket, Address, Contact, Product, ProductsBasket } from '../../models';
import { find } from 'lodash';


@Component({
    moduleId: module.id,
    selector: 'basket-three',
    templateUrl: 'basket-three.component.html',
    styleUrls:['basket-three.css']
})
export class BasketThreeComponent implements OnInit {
    private class: string;
    public listViewHeight: number = 0;
    public address: string;
    public date: string;
    public products: any[] = [];
    public allSum: number = 0;
    public allSumText: string = "";

    constructor(
        public sharedService: SharedService,
        private page: Page,
        private router: RouterExtensions, 
    ) {}

    ngOnInit() { 
        this.page.actionBarHidden = true;  
        this.page.backgroundSpanUnderStatusBar = true;
        if(this.sharedService.basket.address_id != 0){
            var findAddress = <Address>find(this.sharedService.addresses, (v:Address) => v.id == this.sharedService.basket.address_id)
            this.address = `${findAddress.city}, ${findAddress.street}, ${findAddress.house}, ${findAddress.room}`
        }else{
            this.address = `Самовывоз`
        }
        
        
        this.date = `${this.sharedService.basket.date_time.date}, ${this.sharedService.basket.date_time.time}`;

        this.basketInfoToProducts()


        this.listViewHeight = this.sharedService.user.screen_height - 30;
        this.class = (this.sharedService.isIOS) ? 'ios' : 'android';
    }

    finish(){
        this.sharedService.basket.status = 'finish';
        this.sharedService.saveBasket().then(success => {
            this.router.navigate(["index"], {});
            this.sharedService.basket = new Basket()
        });       
    }

    // конвертируем данные о продуктах из корзины в массив
    basketInfoToProducts():any[] {
        // this.products = [];
        let products: any = [];
        this.allSum = 0;

        this.sharedService.basket.products.forEach(v => {
            
            let pr = this.getProductById(v.product_id);

            if(pr != null){
                // products.push({
                //     info: pr,
                //     count: v.product_count,
                //     summa: v.product_count * pr.price
                // })
                this.allSum += v.product_count * pr.price;
            } 
        })
        // console.dump(this.products)
        // this.products = products;
        this.allSumText = `Сумма заказа: ${this.allSum} руб.`
        return this.products;
    } 

        // получаем продукт по его ID
    getProductById(id: number): any{
        return this.sharedService.data.reduce((result, value, key) => {
            let res = find(value['products'], v => v.pid == id)
            if(res != null){
                result = res;
            }
            return result;
        },{})
    }
}
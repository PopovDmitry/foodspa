"use strict";
var core_1 = require("@angular/core");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var page_1 = require("ui/page");
var _1 = require("../../models/");
var services_1 = require("../../services");
var AddressComponent = (function () {
    function AddressComponent(params, page, sharedService) {
        this.params = params;
        this.page = page;
        this.sharedService = sharedService;
        this.errorDisplay = false;
        this.isSubmit = false;
        this.height_padding = 50;
    }
    AddressComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
        this.page.backgroundSpanUnderStatusBar = true;
        if (this.sharedService.isIOS) {
            this.height_padding = this.sharedService.user.screen_height / 1.5;
        }
        if (this.sharedService.isAndroid) {
            this.scroll = this.sharedService.user.screen_height / 2;
        }
        else {
            this.scroll = this.sharedService.user.screen_height;
        }
        if (this.params.context.type == 'add') {
            this.city.nativeElement.text = 'Москва';
            this.street.nativeElement.text = '';
            this.house.nativeElement.text = '';
            this.room.nativeElement.text = '';
            this.name.nativeElement.text = '';
            this.corpus.nativeElement.text = '';
        }
        else {
            this.city.nativeElement.text = this.params.context.data.city;
            this.street.nativeElement.text = this.params.context.data.street;
            this.house.nativeElement.text = this.params.context.data.house;
            this.room.nativeElement.text = this.params.context.data.room;
            this.name.nativeElement.text = this.params.context.data.name;
            this.corpus.nativeElement.text = this.params.context.data.corpus;
        }
    };
    AddressComponent.prototype.close = function () {
        this.params.closeCallback(0);
    };
    AddressComponent.prototype.save = function () {
        var _this = this;
        var address;
        address = new _1.Address(this.params.context.data.id, this.city.nativeElement.text, this.street.nativeElement.text, this.house.nativeElement.text, this.room.nativeElement.text, this.name.nativeElement.text, this.corpus.nativeElement.text);
        if (address.name == "") {
            this.toogleError(true, 'Укажите название адреса доставки');
            this.setBorderColor('name', 'red');
        }
        else if (address.city == "") {
            this.setBorderColor('city', 'red');
            this.toogleError(true, 'Укажите город');
        }
        else if (address.street == "") {
            this.setBorderColor('street', 'red');
            this.toogleError(true, 'Укажите улицу');
        }
        else if (address.house == "") {
            this.setBorderColor('house', 'red');
            this.toogleError(true, 'Укажите дом');
        }
        else if (address.room == "") {
            this.setBorderColor('room', 'red');
            this.toogleError(true, 'Укажите квартиру');
        }
        else {
            this.isSubmit = true;
            this.toogleError(false);
            this.sharedService.api.updateAddress(address)
                .subscribe(function (v) {
                _this.sharedService.addresses[_this.params.context.index] = address;
                setTimeout(function () {
                    _this.isSubmit = false;
                    _this.params.closeCallback({ result: true });
                }, 2000);
            }, function (error) {
                _this.params.closeCallback({ result: false });
            });
        }
    };
    AddressComponent.prototype.delete = function () {
    };
    AddressComponent.prototype.add = function () {
        var _this = this;
        var address;
        address = new _1.Address(0, this.city.nativeElement.text, this.street.nativeElement.text, this.house.nativeElement.text, this.room.nativeElement.text, this.name.nativeElement.text, this.corpus.nativeElement.text);
        if (address.name == "") {
            this.toogleError(true, 'Укажите название адреса доставки');
            this.setBorderColor('name', 'red');
        }
        else if (address.city == "") {
            this.setBorderColor('city', 'red');
            this.toogleError(true, 'Укажите город');
        }
        else if (address.street == "") {
            this.setBorderColor('street', 'red');
            this.toogleError(true, 'Укажите улицу');
        }
        else if (address.house == "") {
            this.setBorderColor('house', 'red');
            this.toogleError(true, 'Укажите дом');
        }
        else if (address.room == "") {
            this.setBorderColor('room', 'red');
            this.toogleError(true, 'Укажите квартиру');
        }
        else {
            this.setBorderColor('room');
            this.isSubmit = true;
            this.toogleError(false);
            this.sharedService.api.createAddress(address)
                .subscribe(function (v) {
                address.id = v.id;
                _this.sharedService.addresses.push(address);
                setTimeout(function () {
                    _this.isSubmit = false;
                    _this.params.closeCallback({ result: true });
                }, 2000);
            }, function (error) {
                _this.params.closeCallback({ result: false });
            });
        }
    };
    AddressComponent.prototype.setBorderColor = function (el, color) {
        if (color === void 0) { color = '#5871b6'; }
        this.city.nativeElement.borderColor = '#5871b6';
        this.street.nativeElement.borderColor = '#5871b6';
        this.house.nativeElement.borderColor = '#5871b6';
        this.room.nativeElement.borderColor = '#5871b6';
        this.name.nativeElement.borderColor = '#5871b6';
        if (this[el]) {
            this[el].nativeElement.borderColor = color;
        }
    };
    AddressComponent.prototype.toogleError = function (display, msg) {
        var _this = this;
        if (display === void 0) { display = false; }
        if (msg === void 0) { msg = "Ошибка сохранения"; }
        if (display) {
            this.errorDisplay = display;
            setTimeout(function () {
                var view = _this.page.getViewById('errorDisplay');
                view.text = msg;
                view.animate({
                    opacity: (display) ? 1 : 0,
                    duration: 1000
                }).then(function () {
                });
            }, 200);
        }
        else {
            var view = this.page.getViewById('errorDisplay');
            if (view != undefined) {
                view.animate({
                    opacity: (display) ? 1 : 0,
                    duration: 1000
                }).then(function () {
                    _this.errorDisplay = display;
                });
            }
        }
    };
    return AddressComponent;
}());
__decorate([
    core_1.ViewChild("city"),
    __metadata("design:type", core_1.ElementRef)
], AddressComponent.prototype, "city", void 0);
__decorate([
    core_1.ViewChild("street"),
    __metadata("design:type", core_1.ElementRef)
], AddressComponent.prototype, "street", void 0);
__decorate([
    core_1.ViewChild("house"),
    __metadata("design:type", core_1.ElementRef)
], AddressComponent.prototype, "house", void 0);
__decorate([
    core_1.ViewChild("room"),
    __metadata("design:type", core_1.ElementRef)
], AddressComponent.prototype, "room", void 0);
__decorate([
    core_1.ViewChild("name"),
    __metadata("design:type", core_1.ElementRef)
], AddressComponent.prototype, "name", void 0);
__decorate([
    core_1.ViewChild("corpus"),
    __metadata("design:type", core_1.ElementRef)
], AddressComponent.prototype, "corpus", void 0);
AddressComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'address',
        templateUrl: 'address.component.html',
        styleUrls: ['address.css']
    }),
    __metadata("design:paramtypes", [modal_dialog_1.ModalDialogParams,
        page_1.Page,
        services_1.SharedService])
], AddressComponent);
exports.AddressComponent = AddressComponent;

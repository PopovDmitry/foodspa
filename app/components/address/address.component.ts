import { Component, OnInit, NgModule, ElementRef, ViewChild } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { Page } from "ui/page";
import { Address } from '../../models/';
import { SharedService } from "../../services";
import label = require("ui/label");


@Component({
    moduleId: module.id,
    selector: 'address',
    templateUrl: 'address.component.html',
    styleUrls: ['address.css']
})
export class AddressComponent implements OnInit {

    @ViewChild("city") city: ElementRef;
    @ViewChild("street") street: ElementRef;
    @ViewChild("house") house: ElementRef;
    @ViewChild("room") room: ElementRef;
    @ViewChild("name") name: ElementRef;
    @ViewChild("corpus") corpus: ElementRef;
    // @ViewChild("err") err: ElementRef;

    private scroll: number;
    private errorDisplay: boolean = false;
    private isSubmit = false;
    private height_padding: number = 50;

    constructor(
        private params: ModalDialogParams, 
        private page: Page,
        private sharedService: SharedService
        ) { 
    }


    ngOnInit() {
        this.page.actionBarHidden = true;  
        this.page.backgroundSpanUnderStatusBar = true;

        if(this.sharedService.isIOS){
            this.height_padding = this.sharedService.user.screen_height / 1.5;
        }
        

        if(this.sharedService.isAndroid){
            this.scroll = this.sharedService.user.screen_height / 2
        }else{
            this.scroll = this.sharedService.user.screen_height
        }
        

        if(this.params.context.type == 'add'){
            this.city.nativeElement.text = 'Москва';
            this.street.nativeElement.text = '' 
            this.house.nativeElement.text = ''
            this.room.nativeElement.text = ''
            this.name.nativeElement.text = ''
            this.corpus.nativeElement.text = ''
        }else{
            //console.dump(this.params.context.data)
            this.city.nativeElement.text = this.params.context.data.city 
            this.street.nativeElement.text = this.params.context.data.street 
            this.house.nativeElement.text = this.params.context.data.house 
            this.room.nativeElement.text = this.params.context.data.room
            this.name.nativeElement.text = this.params.context.data.name 
            this.corpus.nativeElement.text = this.params.context.data.corpus 
        }
        
     }

     // закрыть
     close(){
         this.params.closeCallback(0)
     } 

     // сохранение адреса
     save(){
         let address: Address;
         address = new Address(
            this.params.context.data.id, 
            this.city.nativeElement.text, 
            this.street.nativeElement.text,
            this.house.nativeElement.text,
            this.room.nativeElement.text,
            this.name.nativeElement.text,
            this.corpus.nativeElement.text
         )
          
        if(address.name == ""){
            this.toogleError(true, 'Укажите название адреса доставки')
            // this.name.nativeElement.borderColor = 'red'
            this.setBorderColor('name', 'red');
        }else if(address.city == ""){
            this.setBorderColor('city', 'red');
            this.toogleError(true, 'Укажите город')
        }else if(address.street == ""){
            this.setBorderColor('street', 'red');
            this.toogleError(true, 'Укажите улицу')
        }else if(address.house == ""){
            this.setBorderColor('house', 'red');
            this.toogleError(true, 'Укажите дом')
        }else if(address.room == ""){
            this.setBorderColor('room', 'red');
            this.toogleError(true, 'Укажите квартиру')
        }else{
            this.isSubmit = true;
            this.toogleError(false)
            this.sharedService.api.updateAddress(address)
            .subscribe(v => {
                this.sharedService.addresses[this.params.context.index] = address;
                setTimeout(() => {
                    this.isSubmit = false;
                    this.params.closeCallback({result: true})
                }, 2000)
            }, error => {
                // console.dump(error)
                this.params.closeCallback({result: false})
            })
        }
     }

     // удалить адрес
     delete(){

     }

     // доаблвение адреса
     add(){

         let address: Address;
         address = new Address(
            0, 
            this.city.nativeElement.text, 
            this.street.nativeElement.text,
            this.house.nativeElement.text,
            this.room.nativeElement.text,
            this.name.nativeElement.text,
            this.corpus.nativeElement.text
         )

        //  проверка ввод данных
        if(address.name == ""){
            this.toogleError(true, 'Укажите название адреса доставки')
            // this.name.nativeElement.borderColor = 'red'
            this.setBorderColor('name', 'red');
        }else if(address.city == ""){
            this.setBorderColor('city', 'red');
            this.toogleError(true, 'Укажите город')
        }else if(address.street == ""){
            this.setBorderColor('street', 'red');
            this.toogleError(true, 'Укажите улицу')
        }else if(address.house == ""){
            this.setBorderColor('house', 'red');
            this.toogleError(true, 'Укажите дом')
        }else if(address.room == ""){
            this.setBorderColor('room', 'red');
            this.toogleError(true, 'Укажите квартиру')
        }else{
            this.setBorderColor('room');
            // console.log('-----------------------------1')
            this.isSubmit = true;
            this.toogleError(false)
            // console.log('-----------------------------2')
            this.sharedService.api.createAddress(address)
            .subscribe(v => {
                // console.log('-----------------------------2.1')
                address.id = v.id
                this.sharedService.addresses.push(address)
                // console.log('-----------------------------3')
                setTimeout(() => {
                    this.isSubmit = false;
                    this.params.closeCallback({result: true})
                }, 2000)
                
            }, error => {
                // console.dump(error)
                this.params.closeCallback({result: false})
            })
        }
     }

     setBorderColor(el: string, color: string = '#5871b6'){
        this.city.nativeElement.borderColor = '#5871b6'
        this.street.nativeElement.borderColor = '#5871b6'
        this.house.nativeElement.borderColor = '#5871b6'
        this.room.nativeElement.borderColor = '#5871b6'
        this.name.nativeElement.borderColor = '#5871b6'
        if(this[el]){
            this[el].nativeElement.borderColor = color;
        }  
     }

     toogleError(display: boolean = false, msg: string = "Ошибка сохранения"){
         if(display){
            this.errorDisplay = display;
            setTimeout(()=> {
                let view = <label.Label>this.page.getViewById('errorDisplay')
                view.text = msg;
                view.animate({
                    opacity: (display) ? 1 : 0,
                    duration: 1000
                }).then(() => {

                })    
            }, 200)
         }else{
            let view = this.page.getViewById('errorDisplay')
            if(view != undefined){
                view.animate({
                    opacity: (display) ? 1 : 0,
                    duration: 1000
                }).then(() => {
                    this.errorDisplay = display;
                })
            }
         }
     }
}
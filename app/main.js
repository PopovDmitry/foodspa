"use strict";
var platform_1 = require("nativescript-angular/platform");
var app_module_1 = require("./app.module");
var TnsOneSignal = require('nativescript-onesignal').TnsOneSignal;
var application = require("application");
var core_1 = require("@angular/core");
if (application.ios) {
    var Delegate = (function (_super) {
        __extends(Delegate, _super);
        function Delegate() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Delegate.prototype.applicationDidFinishLaunchingWithOptions = function (nativeApp, launchOptions) {
            try {
                var opts = NSMutableDictionary.new();
                opts.setValueForKey(false, 'kOSSettingsKeyAutoPrompt');
                opts.setValueForKey(false, 'kOSSettingsKeyInAppLaunchURL');
                TnsOneSignal.initWithLaunchOptionsAppIdHandleNotificationReceivedHandleNotificationActionSettings(launchOptions, 'dfefb395-459a-40a3-bbc6-1c00c727dd26', function receivedCallback(notification) {
                    console.log('receivedCallback');
                    console.dump(notification.payload);
                }, function actionCallback(result) {
                    console.log('actionCallback');
                }, opts);
            }
            catch (error) {
                console.error('error', error);
            }
            return true;
        };
        return Delegate;
    }(UIResponder));
    Delegate.ObjCProtocols = [UIApplicationDelegate];
    application.ios.delegate = Delegate;
}
if (application.android) {
    application.on(application.launchEvent, function (args) {
        try {
            TnsOneSignal.startInit(application.android.context).init();
        }
        catch (error) {
            console.error('error', error);
        }
    });
}
core_1.enableProdMode();
platform_1.platformNativeScriptDynamic().bootstrapModule(app_module_1.AppModule);

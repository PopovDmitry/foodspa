import { NgModule } from '@angular/core';
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { MyActionBarComponent, ModalCountComponent, AddressComponent, ContactComponent, DateTimeComponent } from '../../components';


@NgModule({
    imports: [NativeScriptModule],
    exports: [MyActionBarComponent, ModalCountComponent, AddressComponent, ContactComponent, DateTimeComponent],
    declarations:[MyActionBarComponent, ModalCountComponent, AddressComponent, ContactComponent, DateTimeComponent],
    providers: [],
    entryComponents:[
        ModalCountComponent, AddressComponent, ContactComponent, DateTimeComponent
    ]
})
export class SharedModule{}

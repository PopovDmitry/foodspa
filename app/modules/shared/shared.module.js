"use strict";
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var components_1 = require("../../components");
var SharedModule = (function () {
    function SharedModule() {
    }
    return SharedModule;
}());
SharedModule = __decorate([
    core_1.NgModule({
        imports: [nativescript_module_1.NativeScriptModule],
        exports: [components_1.MyActionBarComponent, components_1.ModalCountComponent, components_1.AddressComponent, components_1.ContactComponent, components_1.DateTimeComponent],
        declarations: [components_1.MyActionBarComponent, components_1.ModalCountComponent, components_1.AddressComponent, components_1.ContactComponent, components_1.DateTimeComponent],
        providers: [],
        entryComponents: [
            components_1.ModalCountComponent, components_1.AddressComponent, components_1.ContactComponent, components_1.DateTimeComponent
        ]
    })
], SharedModule);
exports.SharedModule = SharedModule;

import { NgModule } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { ActivationComponent, GetCodeComponent, PushComponent } from '../../components'

const routes = [
    {
        path: "",
        component: ActivationComponent,
    },
    {
        path: 'getCode',
        component: GetCodeComponent
    },
    {
        path: 'push',
        component: PushComponent
    }
];


@NgModule({
    imports: [
        NativeScriptModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forChild(routes),
    ],
    declarations: [ActivationComponent, GetCodeComponent, PushComponent]
})
export class ActivationModule { }
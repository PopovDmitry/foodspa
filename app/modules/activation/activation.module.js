"use strict";
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var router_1 = require("nativescript-angular/router");
var components_1 = require("../../components");
var routes = [
    {
        path: "",
        component: components_1.ActivationComponent,
    },
    {
        path: 'getCode',
        component: components_1.GetCodeComponent
    },
    {
        path: 'push',
        component: components_1.PushComponent
    }
];
var ActivationModule = (function () {
    function ActivationModule() {
    }
    return ActivationModule;
}());
ActivationModule = __decorate([
    core_1.NgModule({
        imports: [
            nativescript_module_1.NativeScriptModule,
            router_1.NativeScriptRouterModule,
            router_1.NativeScriptRouterModule.forChild(routes),
        ],
        declarations: [components_1.ActivationComponent, components_1.GetCodeComponent, components_1.PushComponent]
    })
], ActivationModule);
exports.ActivationModule = ActivationModule;

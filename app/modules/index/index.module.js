"use strict";
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var router_1 = require("nativescript-angular/router");
var components_1 = require("../../components");
var angular_1 = require("nativescript-telerik-ui/sidedrawer/angular");
var shared_module_1 = require("../shared/shared.module");
var routes = [
    {
        path: "",
        component: components_1.IndexComponent,
    },
    {
        path: "category/:cat",
        component: components_1.CategoryComponent
    },
    {
        path: "category/:cat/view/:id",
        component: components_1.DetailComponent
    }
];
var IndexModule = (function () {
    function IndexModule() {
    }
    return IndexModule;
}());
IndexModule = __decorate([
    core_1.NgModule({
        imports: [
            nativescript_module_1.NativeScriptModule,
            router_1.NativeScriptRouterModule,
            router_1.NativeScriptRouterModule.forChild(routes),
            shared_module_1.SharedModule
        ],
        exports: [],
        declarations: [
            components_1.IndexComponent,
            components_1.IndexItemComponent,
            components_1.CategoryComponent,
            angular_1.SIDEDRAWER_DIRECTIVES,
            components_1.DetailComponent,
            components_1.FilterComponent
        ],
        providers: [],
        entryComponents: []
    })
], IndexModule);
exports.IndexModule = IndexModule;

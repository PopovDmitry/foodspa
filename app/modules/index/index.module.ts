import { NgModule } from '@angular/core';
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { IndexComponent, 
            // MyActionBarComponent, 
            IndexItemComponent, 
            CategoryComponent,
            DetailComponent,
            FilterComponent
         } from '../../components';
import { SIDEDRAWER_DIRECTIVES } from "nativescript-telerik-ui/sidedrawer/angular";
// import { SlidesModule } from 'nativescript-ng2-slides';
import { SharedModule } from '../shared/shared.module';

const routes = [
    {
        path: "",
        component: IndexComponent,
    },
    {
        path: "category/:cat",
        component: CategoryComponent
    },
    {
        path: "category/:cat/view/:id",
        component: DetailComponent
    }
];

@NgModule({
    imports: [
        NativeScriptModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forChild(routes),
        // SlidesModule,
        SharedModule
    ],
    exports: [
        // MyActionBarComponent
    ],
    declarations: [
        IndexComponent, 
        // MyActionBarComponent, 
        IndexItemComponent,
        CategoryComponent,
        SIDEDRAWER_DIRECTIVES,
        DetailComponent,
        FilterComponent
        ],
    providers: [],
    entryComponents:[
        // ModalCountComponent
    ]
})
export class IndexModule { }

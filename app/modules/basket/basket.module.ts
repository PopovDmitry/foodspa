import { NgModule } from '@angular/core';
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { BasketOneComponent, BasketTwoComponent, BasketThreeComponent } from '../../components';
// import { IndexModule } from '../index/index.module'
import { SharedModule } from '../shared/shared.module';

const routes = [
    {
        path: "", 
        component: BasketOneComponent,
    },
    {
        path: "two", 
        component: BasketTwoComponent,
    },
    {
        path: "three", 
        component: BasketThreeComponent,
    } 
];

@NgModule({
    imports: [
        NativeScriptModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forChild(routes), 
        SharedModule 
    ],
    exports: [],
    declarations: [
        BasketOneComponent,
        BasketTwoComponent,
        BasketThreeComponent
    ],
    providers: [],
})
export class BasketModule { }

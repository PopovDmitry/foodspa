"use strict";
var core_1 = require("@angular/core");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var router_1 = require("nativescript-angular/router");
var components_1 = require("../../components");
var shared_module_1 = require("../shared/shared.module");
var routes = [
    {
        path: "",
        component: components_1.BasketOneComponent,
    },
    {
        path: "two",
        component: components_1.BasketTwoComponent,
    },
    {
        path: "three",
        component: components_1.BasketThreeComponent,
    }
];
var BasketModule = (function () {
    function BasketModule() {
    }
    return BasketModule;
}());
BasketModule = __decorate([
    core_1.NgModule({
        imports: [
            nativescript_module_1.NativeScriptModule,
            router_1.NativeScriptRouterModule,
            router_1.NativeScriptRouterModule.forChild(routes),
            shared_module_1.SharedModule
        ],
        exports: [],
        declarations: [
            components_1.BasketOneComponent,
            components_1.BasketTwoComponent,
            components_1.BasketThreeComponent
        ],
        providers: [],
    })
], BasketModule);
exports.BasketModule = BasketModule;

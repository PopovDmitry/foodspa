import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { AppRoutingModule } from "./app.routing";
import { AppComponent } from "./app.component";
import { LoadingComponent, ErrorGooglePLayComponent } from './components';
import { ApiService, SharedService, ImageService } from './services';
import { ModalDialogService } from "nativescript-angular/modal-dialog";
// import { SharedModule } from './modules/shared/shared.module';

import { NativeScriptHttpModule } from "nativescript-angular/http";
// import {HTTP_} from "@angular/http";

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        // NativeScriptModule,
        AppRoutingModule, 
        NativeScriptHttpModule
        // SharedModule
    ],
    declarations: [
        AppComponent,
        LoadingComponent,
        ErrorGooglePLayComponent,
    ],
    providers: [
        SharedService,
        ApiService,
        ImageService,
        ModalDialogService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})

export class AppModule { }

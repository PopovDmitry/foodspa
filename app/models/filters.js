"use strict";
var Filter = (function () {
    function Filter(title, name, image, display, order) {
        if (order === void 0) { order = 1; }
        this.title = title;
        this.name = name;
        this.image = image;
        this.display = display;
        this.order = order;
    }
    return Filter;
}());
exports.Filter = Filter;

"use strict";
var DateTime = (function () {
    function DateTime(date, time) {
        this.date = date;
        this.time = time;
    }
    return DateTime;
}());
exports.DateTime = DateTime;

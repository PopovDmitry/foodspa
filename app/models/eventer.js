"use strict";
var Eventer = (function () {
    function Eventer(type, data) {
        this.type = type;
        this.data = data;
    }
    return Eventer;
}());
exports.Eventer = Eventer;

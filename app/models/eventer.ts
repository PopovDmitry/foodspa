export class Eventer{
    constructor(
        public type: string, // тип события
        public data: any, // содержание события
    ){}
}
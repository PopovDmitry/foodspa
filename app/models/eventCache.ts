import imageSource = require("image-source");
export class EventCacheModel {
    constructor(
        public url: string, // url
        public source: imageSource.ImageSource, // изображение
        public is_cache: boolean
    ){

    }
}
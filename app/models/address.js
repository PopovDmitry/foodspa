"use strict";
var Address = (function () {
    function Address(id, city, street, house, room, name, corpus) {
        this.id = id;
        this.city = city;
        this.street = street;
        this.house = house;
        this.room = room;
        this.name = name;
        this.corpus = corpus;
    }
    return Address;
}());
exports.Address = Address;

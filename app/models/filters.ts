export class Filter {
    constructor(
        public title: string, // русское название
        public name: string, // название фильтра на англ
        public image: any, // изображение
        public display: boolean, // вкл/выкл
        public order: number = 1, // сортировка
    ){}
}
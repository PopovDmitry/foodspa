export class Address{
    constructor(
        public id: number, // id адреса доставки
        // public delivery: boolean = true, // доставка включена
        public city?: string, // город
        public street?: string, // улица
        public house?: string, // дом
        public room?: string, // кв
        public name?: string, // название
        public corpus?: string, // корпус
    ){}
}
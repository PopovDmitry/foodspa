"use strict";
var Category = (function () {
    function Category(name, title, title_small, image, order, display, color, filters, products) {
        this.name = name;
        this.title = title;
        this.title_small = title_small;
        this.image = image;
        this.order = order;
        this.display = display;
        this.color = color;
        this.filters = filters;
        this.products = products;
    }
    return Category;
}());
exports.Category = Category;

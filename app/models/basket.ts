import { ProductsBasket, Address, DateTime, Contact } from './';

export class Basket{ 
    constructor(
        public products: ProductsBasket[] = [], // продукты в корзине
        public uuid?: string, // индификатор заказа
        public status?: string, // статус заказа
        public address_id?: number, // адрес доставки
        public contact_id?: number, // контакт доставки
        public date_time?: DateTime, // дата и время доставки
        public comment?: string, // коментарий к заказу
    ){
 
    }
}
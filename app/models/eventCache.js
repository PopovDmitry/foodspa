"use strict";
var EventCacheModel = (function () {
    function EventCacheModel(url, source, is_cache) {
        this.url = url;
        this.source = source;
        this.is_cache = is_cache;
    }
    return EventCacheModel;
}());
exports.EventCacheModel = EventCacheModel;

export class Error{
    constructor(
        public type: string, // тип ошибки
        public code: number, // код ошибки
        public msg: string, // сообщение об ошибки
        public uuid: string, // индификатор устроства
        public data: any, // данные об ошибке
    ){

    }
}
export class Contact{
    constructor(
        public id: number,
        public fio: string, // ФИО заказчика
        public phone: number, // Телефон заказчика
        public email: string, // email заказчка
        public name: string // название контакта
    ){

    }
}
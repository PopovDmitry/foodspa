"use strict";
var Error = (function () {
    function Error(type, code, msg, uuid, data) {
        this.type = type;
        this.code = code;
        this.msg = msg;
        this.uuid = uuid;
        this.data = data;
    }
    return Error;
}());
exports.Error = Error;

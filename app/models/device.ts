export class Device{
    constructor(
        public app_phone: number, // номер на который зарегистрованно приложение
        public app_send_sms: boolean = false, // отправленно ли смс
        public app_sms_code: number, // код в СМС
        public app_valid_sms: boolean = false, // указаени было ли проверенно смс
        public os_name: string, // название платформы
        public os_version: string, // версия платформы
        public sdk_version: string, // версия SDK
        public device_model: string, // Модель устройства
        public device_type: string, // Тип устройства,
        public screen_width: number, // ширина экрана,
        public screen_height: number, // высота экарана
        public uuid: string, // UUID устройства
        public push: boolean = false, // разрешено ли использовать PUSH уведомления
    ){}
}
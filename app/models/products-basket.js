"use strict";
var ProductsBasket = (function () {
    function ProductsBasket(product_id, product_count) {
        this.product_id = product_id;
        this.product_count = product_count;
    }
    return ProductsBasket;
}());
exports.ProductsBasket = ProductsBasket;

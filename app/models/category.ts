import {Filter, Product} from './';
export class Category{
    constructor(
        public name: string, // англ название категории
        public title: string, // русское название категории
        public title_small: string, // второй текст
        public image: any, // картинка для категории,
        public order: number, // сортировка
        public display: boolean, // указатель включен ли раздел
        public color: string, // цвет текста
        public filters: Filter[], // фильтры
        public products: Product[] // Продукты
    ){}
}
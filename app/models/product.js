"use strict";
var Product = (function () {
    function Product(pid, name, title, image, price, description, description_small, filters, params, features, display, order) {
        if (display === void 0) { display = true; }
        this.pid = pid;
        this.name = name;
        this.title = title;
        this.image = image;
        this.price = price;
        this.description = description;
        this.description_small = description_small;
        this.filters = filters;
        this.params = params;
        this.features = features;
        this.display = display;
        this.order = order;
    }
    return Product;
}());
exports.Product = Product;

export class Product{
    constructor(
        public pid: number, // ID товара из битрикса
        public name: string, // название продукта на англ
        public title: string, // название продукта на русском
        public image: any, // изображение 
        public price: number, // цена товара
        public description: string, // описание товара
        public description_small: string, // краткое описание товара описание товара
        public filters: any[], // каким фильтрам пренадлежит
        public params: any[], // массив параметров
        public features: string, // полезные свойства
        public display: boolean = true, // вкл/выкл продукта
        public order: number, // сортировка
    ){

    }
}
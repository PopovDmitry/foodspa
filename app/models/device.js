"use strict";
var Device = (function () {
    function Device(app_phone, app_send_sms, app_sms_code, app_valid_sms, os_name, os_version, sdk_version, device_model, device_type, screen_width, screen_height, uuid, push) {
        if (app_send_sms === void 0) { app_send_sms = false; }
        if (app_valid_sms === void 0) { app_valid_sms = false; }
        if (push === void 0) { push = false; }
        this.app_phone = app_phone;
        this.app_send_sms = app_send_sms;
        this.app_sms_code = app_sms_code;
        this.app_valid_sms = app_valid_sms;
        this.os_name = os_name;
        this.os_version = os_version;
        this.sdk_version = sdk_version;
        this.device_model = device_model;
        this.device_type = device_type;
        this.screen_width = screen_width;
        this.screen_height = screen_height;
        this.uuid = uuid;
        this.push = push;
    }
    return Device;
}());
exports.Device = Device;

"use strict";
var Basket = (function () {
    function Basket(products, uuid, status, address_id, contact_id, date_time, comment) {
        if (products === void 0) { products = []; }
        this.products = products;
        this.uuid = uuid;
        this.status = status;
        this.address_id = address_id;
        this.contact_id = contact_id;
        this.date_time = date_time;
        this.comment = comment;
    }
    return Basket;
}());
exports.Basket = Basket;

"use strict";
var Contact = (function () {
    function Contact(id, fio, phone, email, name) {
        this.id = id;
        this.fio = fio;
        this.phone = phone;
        this.email = email;
        this.name = name;
    }
    return Contact;
}());
exports.Contact = Contact;

"use strict";
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var components_1 = require("./components");
var routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: '/loading'
    },
    {
        path: "loading",
        pathMatch: "full",
        component: components_1.LoadingComponent
    },
    {
        path: "activation",
        loadChildren: function () { return require("./modules/activation/activation.module")["ActivationModule"]; }
    },
    {
        path: "index",
        loadChildren: function () { return require("./modules/index/index.module")["IndexModule"]; }
    },
    {
        path: "basket",
        loadChildren: function () { return require("./modules/basket/basket.module")["BasketModule"]; }
    },
    {
        path: "errorGooglePLay",
        component: components_1.ErrorGooglePLayComponent
    }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    core_1.NgModule({
        imports: [router_1.NativeScriptRouterModule.forRoot(routes)],
        exports: [router_1.NativeScriptRouterModule]
    })
], AppRoutingModule);
exports.AppRoutingModule = AppRoutingModule;

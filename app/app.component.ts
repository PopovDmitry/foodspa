import { Component, OnInit } from "@angular/core";
// import { ApiService } from "./services/api.service";
import {initializeOnAngular} from "nativescript-web-image-cache";


@Component({
    selector: "ns-app",
    templateUrl: "app.component.html",
})
export class AppComponent implements OnInit { 

    constructor(){}
    
    ngOnInit(){
        initializeOnAngular();
    }
}

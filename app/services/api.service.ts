import { Observable, Subject } from "rxjs/Rx";
import { Device, Category, Eventer, Filter, Product, Error, Basket, Address, Contact } from '../models/';
import { Injectable, NgZone } from "@angular/core";
import { Http, Response, RequestOptions, Headers } from "@angular/http";
import platformModule = require("platform");
import { SharedService} from './shared.service';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';



@Injectable()
export class ApiService{

    public firebase: any;
    public isConnect: boolean = false;
    public loginData: any;
    public cfg: any;
    public observable: Subject<any>;
    public serverUrl = "http://foodspa.ru:9090/";

    constructor(
        private ngZone: NgZone,
        private sharedService: SharedService,
        private http: Http
    ){
        this.sharedService.serverUrl = this.serverUrl;
    }

    initObservable(): Subject<any>{
        this.observable = new Subject();
        return this.observable;
    }

    // проверка существование девайса
    issetDevice(uuid: string): Observable<any>{
        return this.get('devices/uuid/' + uuid);
    }

    // проверка существование номер
    registrationDevice(device: Device): Observable<any>{
        return this.post('devices/registration', device);
    }

    // проверка кода
    checkCode(app_sms_code: number){
        return this.get('devices/app_sms_code/' + app_sms_code);
    }

    // загрузка всех данных
    loadAll(){
        return this.get('all');
    }

    // создание нового заказа
    createOrder(data: Basket){
        return this.post('order/create', data);
    }

    // обновление данных по заказу
    updateBasket(data: Basket){
        return this.post('order/update', data);
    }

    // создаем адрес
    createAddress(data: Address){
        return this.post('api/createAddress', data);
    }

    // обновляем адрес
    updateAddress(data: Address){
        return this.post('api/updateAddress', data);
    }


    // создаем контакт
    createContact(data: Contact){
        return this.post('api/createContact', data);
    }

    // обновляем контакт
    updateContact(data: Contact){
        return this.post('api/updateContact', data);
    }

    // resend sms CODE
    resendCode(app_phone: number){
        return this.post('api/resend', {
            app_phone: app_phone
        });
    }

    get(method: string): Observable<any>{
        let url: string = this.serverUrl + method;
        return this.http
                .get(url, this.createRequestOptions())
                .map((res: Response) => {
                    return res.json();
                })
                .catch(this.handleError)
    }

    // обновление
    put(method: string, data: any): Observable<any>{
        let url: string = this.serverUrl + method;
        console.log(url);
        return this.http
                .put(url, {data})
                .map((res: Response) => {
                    return res.json();
                })
                .catch(this.handleError)
    }

    // Insert
    post(method: string, data: any): Observable<any>{
        let url: string = this.serverUrl + method;
        return this.http
                .post(url, {data}, this.createRequestOptions())
                .map((res: Response) => {
                    return res.json();
                })
                .catch(this.handleError)
    }

     private createRequestOptions() {
        let headers = new Headers(); 
        headers.append("Device-UUID", this.sharedService.uuid);
        // headers.append("AuthToken", "my-token");

        headers.append("Content-Type", "application/json");
        let options = new RequestOptions({ headers: headers });
        return options;
    }

    

    handleError (errorHttp: Response | any) {
        let errMsg: string;
        let errorSend: Error;

        
        if (errorHttp instanceof Response) {
            const body = errorHttp.json() || '';
            const err = body.error || JSON.stringify(body);
            // errMsg = `${errorHttp.status} - ${errorHttp.statusText || ''} ${err}`;
            
            errorSend = new Error('http', errorHttp.status, err, '',
                {
                    url: errorHttp.url 
                }
            )
        } else {
            errMsg = errorHttp.message ? errorHttp.message : errorHttp.toString();
            errorSend = new Error(
                'http',
                0,
                errMsg,
                '',
                {}
            )
        }
        
        return Observable.throw(errorSend);
    }

    // отправка данных в общий обсервебал
    sendNext(name: string, data: any): void{
        this.sharedService.observable.next(new Eventer(name, data))
    }

}
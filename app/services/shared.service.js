"use strict";
var core_1 = require("@angular/core");
var Rx_1 = require("rxjs/Rx");
var _1 = require("../models/");
var config_1 = require("../config");
var platformModule = require("platform");
var SharedService = (function () {
    function SharedService() {
        this.data = [];
        this.eventTypes = config_1.EventsTypes;
        this.isIOS = (platformModule.isIOS) ? true : false;
        this.isAndroid = (platformModule.isAndroid) ? true : false;
        this.fullLoad = [
            this.eventTypes.api.init,
            this.eventTypes.api.login,
            this.eventTypes.api.categories,
            this.eventTypes.api.cfg,
            this.eventTypes.api.user,
            this.eventTypes.api.filters,
        ];
        this.uuid = platformModule.device.uuid;
        this.loaded = [];
        this.isLoad = false;
        this.contacts = [];
        this.addresses = [];
    }
    SharedService.prototype.initObservable = function () {
        this.observable = new Rx_1.Subject();
        return this.observable;
    };
    SharedService.prototype.sendNext = function (name, data) {
        this.observable.next(new _1.Eventer(name, data));
    };
    SharedService.prototype.addBasket = function (product_id, product_count) {
        var _this = this;
        if (product_count === void 0) { product_count = 1; }
        if (this.basket == undefined || this.basket.uuid == undefined) {
            this.basket = new _1.Basket([new _1.ProductsBasket(product_id, product_count)]);
            this.basket.address_id = 0;
            this.api.createOrder(this.basket).subscribe(function (v) {
                _this.basket.uuid = v.uuid;
            }, function (error) {
            });
        }
        else {
            if (this.basket.products.length > 0) {
                var find = this.basket.products.filter(function (v) { return v.product_id == product_id; });
                if (find.length > 0) {
                    find[0].product_count = find[0].product_count + product_count;
                }
                else {
                    this.basket.products.push(new _1.ProductsBasket(product_id, product_count));
                }
            }
            else {
                this.basket.products.push(new _1.ProductsBasket(product_id, product_count));
            }
            this.saveBasket().then(function (s) {
            }, function (e) {
            });
        }
        this.sendNext(this.eventTypes.actions.addbasket, true);
    };
    SharedService.prototype.saveBasket = function () {
        var _this = this;
        var basket = this.basket;
        return new Promise(function (resolve, reject) {
            _this.api.updateBasket(basket).subscribe(function (v) {
                resolve(true);
            }, function (error) {
                reject(false);
            });
        });
    };
    return SharedService;
}());
SharedService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], SharedService);
exports.SharedService = SharedService;

import { Injectable } from '@angular/core';
import imageCacheModule = require("ui/image-cache");
import imageSource = require("image-source");
import fs = require("file-system");
import { Observable, Subject, Notification } from "rxjs/Rx";
import { Eventer,  EventCacheModel} from '../models/';
import 'rxjs/add/observable/of';

// class EventCacheModel {
//     constructor(
//         public url: string, // url
//         public source: imageSource.ImageSource, // изображение
//         public is_cache: boolean
//     ){

//     }
// }


@Injectable()
export class ImageService{

    public cache;
    private placeholderImage: string = '../images/logo.png';
    private maxRequests: number = 5;
    public observable: Subject<any>; // канал сабытий

    constructor() { 
        this.cache = new imageCacheModule.Cache();
        this.cache.placeholder = this.setPlaceHolder();
        this.cache.maxRequests = this.maxRequests;
        this.initObservable();
        // console.log('this.cache.placeholder',this.cache.placeholder)
    }

    // Основной обсервебл
    initObservable(): Subject<any>{
        this.observable = new Subject();
        return this.observable;
    }

    setPlaceHolder(){
        return imageSource.fromFile(fs.path.join(__dirname, this.placeholderImage))
    }

    // добавляем в очередь кеширование изображений
    imageSrc(url: string){
        this.cache.enableDownload();
        var imgSouce: imageSource.ImageSource;

        var image = this.cache.get(url);
        if (image) { // Если есть в кеше
            imgSouce = imageSource.fromNativeSource(image);
            this.observable.next(new EventCacheModel(url, imgSouce, true))
        } else {
            this.cache.push({
                key: url,
                url: url,
                completed: (image: any, key: string) => {
                    if (url === key) {
                        imgSouce = imageSource.fromNativeSource(image);
                        this.observable.next(new EventCacheModel(url, imgSouce, false));
                    }
                }
            });
        }

        this.cache.disableDownload();
    }

    

    // загрузка всех изображений
    loadAll(urls: string[]): Promise<any>{
        this.cache.enableDownload();
        return new Promise((resolveMain, reject) => {
            let promises = [];
            urls.forEach((url, k) => {
                let promise  = new Promise((resolve, reject) => {
                    var imgSouce: imageSource.ImageSource;
                    var image = this.cache.get(url);
                    if (image) { // Если есть в кеше
                        imgSouce = imageSource.fromNativeSource(image);
                        resolve(new EventCacheModel(url, imgSouce, false))
                    }else{
                        this.cache.push({
                            key: url,
                            url: url,
                            completed: (image: any, key: string) => {
                                if (url === key) {
                                    imgSouce = imageSource.fromNativeSource(image);
                                    resolve(new EventCacheModel(url, imgSouce, false))
                                }
                            }
                        });
                    }
                })
                promises.push(promise)
            })

            Promise.all(promises).then((res) => {
                this.cache.disableDownload();
                resolveMain(res);
            })
        })

        
    }

    

    
}
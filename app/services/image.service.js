"use strict";
var core_1 = require("@angular/core");
var imageCacheModule = require("ui/image-cache");
var imageSource = require("image-source");
var fs = require("file-system");
var Rx_1 = require("rxjs/Rx");
var _1 = require("../models/");
require("rxjs/add/observable/of");
var ImageService = (function () {
    function ImageService() {
        this.placeholderImage = '../images/logo.png';
        this.maxRequests = 5;
        this.cache = new imageCacheModule.Cache();
        this.cache.placeholder = this.setPlaceHolder();
        this.cache.maxRequests = this.maxRequests;
        this.initObservable();
    }
    ImageService.prototype.initObservable = function () {
        this.observable = new Rx_1.Subject();
        return this.observable;
    };
    ImageService.prototype.setPlaceHolder = function () {
        return imageSource.fromFile(fs.path.join(__dirname, this.placeholderImage));
    };
    ImageService.prototype.imageSrc = function (url) {
        var _this = this;
        this.cache.enableDownload();
        var imgSouce;
        var image = this.cache.get(url);
        if (image) {
            imgSouce = imageSource.fromNativeSource(image);
            this.observable.next(new _1.EventCacheModel(url, imgSouce, true));
        }
        else {
            this.cache.push({
                key: url,
                url: url,
                completed: function (image, key) {
                    if (url === key) {
                        imgSouce = imageSource.fromNativeSource(image);
                        _this.observable.next(new _1.EventCacheModel(url, imgSouce, false));
                    }
                }
            });
        }
        this.cache.disableDownload();
    };
    ImageService.prototype.loadAll = function (urls) {
        var _this = this;
        this.cache.enableDownload();
        return new Promise(function (resolveMain, reject) {
            var promises = [];
            urls.forEach(function (url, k) {
                var promise = new Promise(function (resolve, reject) {
                    var imgSouce;
                    var image = _this.cache.get(url);
                    if (image) {
                        imgSouce = imageSource.fromNativeSource(image);
                        resolve(new _1.EventCacheModel(url, imgSouce, false));
                    }
                    else {
                        _this.cache.push({
                            key: url,
                            url: url,
                            completed: function (image, key) {
                                if (url === key) {
                                    imgSouce = imageSource.fromNativeSource(image);
                                    resolve(new _1.EventCacheModel(url, imgSouce, false));
                                }
                            }
                        });
                    }
                });
                promises.push(promise);
            });
            Promise.all(promises).then(function (res) {
                _this.cache.disableDownload();
                resolveMain(res);
            });
        });
    };
    return ImageService;
}());
ImageService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], ImageService);
exports.ImageService = ImageService;

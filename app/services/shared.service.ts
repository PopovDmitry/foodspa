import { Injectable } from '@angular/core';
import { Observable, Subject } from "rxjs/Rx";
import { Device, Category, Eventer, Basket, ProductsBasket, Address, Contact } from '../models/';
import { EventsTypes } from '../config';
import platformModule = require("platform");


// основной хранитель данных

@Injectable()
export class SharedService {

    public observable: Subject<any>; // основной канал сабытий
    public user: Device; // иформация о пользователе
    
    public data: Category[] = []; // данные о категориях 

    public eventTypes = EventsTypes; // типы событий
    // public filters: Filter[] = []; // данные о фильтрах 
    // public products: Product[] = []; // данные о продуктах
    public isIOS: boolean = (platformModule.isIOS) ? true: false; // указатель IOS
    public isAndroid: boolean = (platformModule.isAndroid) ? true: false; // указатель Android
    public fullLoad: any[] = [ // набор событий, после срабатывания которого считается полная загрузка данных
        this.eventTypes.api.init,
        this.eventTypes.api.login,
        this.eventTypes.api.categories,
        this.eventTypes.api.cfg,
        this.eventTypes.api.user,
        this.eventTypes.api.filters,
        //this.eventTypes.api.products,
    ];
    public uuid: string = platformModule.device.uuid; // uuid приложения
    public basket: Basket; // корзина
    public loaded: any[] = []
    public isLoad: boolean = false; // индикатор полной загрузки
    public serverUrl: string; // url сервера из apiService
    public api: any;

    public contacts: Contact[] = [];
    public addresses: Address[] = [];




    constructor(
        
    ) {

     }
    
    // Основной обсервебл
    initObservable(): Subject<any>{
        this.observable = new Subject();
        return this.observable;
    }

    // отправка данных в общий обсервебал
    sendNext(name: string, data: any): void{
        this.observable.next(new Eventer(name, data))
    }

    // добавление в корзинку товара
    addBasket(product_id: number, product_count: number = 1){
        // проверяем наличие данных уже в корзине
        
        // если ничего не создано, создаем запись в карзине
        if(this.basket == undefined || this.basket.uuid == undefined){
            this.basket = new Basket(
                [new ProductsBasket(product_id, product_count)]
            )

            this.basket.address_id = 0;

            this.api.createOrder(this.basket).subscribe(v => {
                this.basket.uuid = v.uuid;
            }, error => {
                // console.log('ERRROR ADD BASKET')
                // console.dump(error);
            })

            // this.callMethodFromApi('createOrder', this.basket);

        }else{
            // проверяем наличие товаров
            if(this.basket.products.length > 0){
                // проверяем наличе подобных товаро и если есть то добвляем
                let find = this.basket.products.filter( v => v.product_id == product_id)
                if(find.length > 0){
                    find[0].product_count = find[0].product_count + product_count; 
                }else{
                    this.basket.products.push(new ProductsBasket(product_id, product_count))
                }
            }else{
                this.basket.products.push(new ProductsBasket(product_id, product_count))
            }

            // обновлем данные на сервер
            this.saveBasket().then(s => {

            }, e => {

            });
        }

        this.sendNext(this.eventTypes.actions.addbasket, true);
        // console.dump(this.basket)
    }

    // обновление данных в корзине
    saveBasket(): Promise<any>{
        var basket = this.basket;
        // console.dump(basket)
        return new Promise((resolve, reject) =>{
            this.api.updateBasket(basket).subscribe(v => {
                resolve(true)
            }, error => {
                reject(false) 
                
                // console.dump(error)
            })
        })
        
    }

    // callMethodFromApi(methodName, data:any = {}){
    //     this.sendNext(this.eventTypes.actions.callApiMethod, {
    //         method: methodName,
    //         data: data
    //     })
    // }
}
"use strict";
var core_1 = require("@angular/core");
var FilterPipe = (function () {
    function FilterPipe() {
    }
    FilterPipe.prototype.transform = function (array, args) {
        if (array) {
            var orderByValue_1 = args[0];
            var byVal_1 = 1;
            if (orderByValue_1.charAt(0) == "!") {
                byVal_1 = -1;
                orderByValue_1 = orderByValue_1.substring(1);
            }
            console.log("byVal", byVal_1);
            console.log("orderByValue", orderByValue_1);
            array.sort(function (a, b) {
                if (a[orderByValue_1] < b[orderByValue_1]) {
                    return -1 * byVal_1;
                }
                else if (a[orderByValue_1] > b[orderByValue_1]) {
                    return 1 * byVal_1;
                }
                else {
                    return 0;
                }
            });
            return array;
        }
    };
    return FilterPipe;
}());
FilterPipe = __decorate([
    core_1.Pipe({
        name: 'filter'
    })
], FilterPipe);
exports.FilterPipe = FilterPipe;

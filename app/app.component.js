"use strict";
var core_1 = require("@angular/core");
var nativescript_web_image_cache_1 = require("nativescript-web-image-cache");
var AppComponent = (function () {
    function AppComponent() {
    }
    AppComponent.prototype.ngOnInit = function () {
        nativescript_web_image_cache_1.initializeOnAngular();
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: "ns-app",
        templateUrl: "app.component.html",
    }),
    __metadata("design:paramtypes", [])
], AppComponent);
exports.AppComponent = AppComponent;

import { platformNativeScriptDynamic } from "nativescript-angular/platform";
import { AppModule } from "./app.module";
var TnsOneSignal = require('nativescript-onesignal').TnsOneSignal;
import * as application from 'application';
import {enableProdMode} from '@angular/core';



if (application.ios) {
    // console.log('1')
	class Delegate extends UIResponder implements UIApplicationDelegate {
		public static ObjCProtocols = [UIApplicationDelegate]
		applicationDidFinishLaunchingWithOptions(nativeApp: UIApplication, launchOptions: NSDictionary<any, any>): boolean {
			try {
                let opts = NSMutableDictionary.new()
                opts.setValueForKey(false, 'kOSSettingsKeyAutoPrompt')
		        opts.setValueForKey(false, 'kOSSettingsKeyInAppLaunchURL')
                // opts.setValueForKey(OSNotificationDisplayType.None, 'kOSSettingsKeyInFocusDisplayOption')

				// var res = TnsOneSignal.initWithLaunchOptionsAppId(launchOptions, 'dfefb395-459a-40a3-bbc6-1c00c727dd26')
                TnsOneSignal.initWithLaunchOptionsAppIdHandleNotificationReceivedHandleNotificationActionSettings(
                    launchOptions,
                    'dfefb395-459a-40a3-bbc6-1c00c727dd26',
                    function receivedCallback(notification) {
                        console.log('receivedCallback')
                        console.dump(notification.payload);
                    },
                    function actionCallback(result) {
                        console.log('actionCallback')
                    },
                    opts

                )
			} catch (error) {
				console.error('error', error)
			}

            // console.log('2')
			return true
		}
	}
    // console.log('3')
	application.ios.delegate = Delegate
}


if (application.android) {
	application.on(application.launchEvent, function(args: application.ApplicationEventData) {
		try {
			TnsOneSignal.startInit(application.android.context).init()
		} catch (error) {
			console.error('error', error)
		}
	})
}

enableProdMode();

platformNativeScriptDynamic().bootstrapModule(AppModule);
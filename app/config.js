"use strict";
exports.EventsTypes = {
    api: {
        init: "api.init",
        login: "api.login",
        user: "api.user",
        categories: "api.categories",
        cfg: "api.cfg",
        userNotFound: "api.userNotFound",
        filters: 'api.filters',
        products: 'api.products',
        start: 'api.start',
    },
    error: {
        null: 'error.null',
        'undefined': 'error.undefined',
        http: 'error.http',
    },
    actions: {
        loaded: 'api.loaded',
        redirect: 'action.redirect',
        openMenu: 'action.openMenu',
        addbasket: 'action.addBasket',
        callApiMethod: 'action.callApiMethod',
    }
};
exports.CONFIG = {
    storage: {
        dbname: 'foodspa',
    }
};

export const EventsTypes = {
    api: {
        init: "api.init", // инициализация api
        login: "api.login", // авторизция
        user: "api.user", // данные пользовтеля
        categories: "api.categories", // данные по каталогу
        cfg: "api.cfg", // данные по конфигам
        userNotFound: "api.userNotFound", // пользователь не найден,
        filters: 'api.filters', // фильтры
        products: 'api.products', // продукты
        start: 'api.start', // событие по которому начинается загрузка данных
    },
    error: {
        null: 'error.null', // ошибка null
        'undefined': 'error.undefined', // ошибка undefined
        http: 'error.http', // ошибка HTTP запроса
    },
    actions:{
        loaded: 'api.loaded', // указание что все загружено
        redirect: 'action.redirect', // было перенаправление
        openMenu: 'action.openMenu', // указаение открытие меню
        addbasket: 'action.addBasket', // добавление в корзину
        callApiMethod: 'action.callApiMethod', // вызов метода из апи
    }
}

export const CONFIG = {
    storage: {
        dbname: 'foodspa',
    }
};
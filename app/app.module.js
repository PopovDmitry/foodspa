"use strict";
var core_1 = require("@angular/core");
var app_routing_1 = require("./app.routing");
var app_component_1 = require("./app.component");
var components_1 = require("./components");
var services_1 = require("./services");
var modal_dialog_1 = require("nativescript-angular/modal-dialog");
var http_1 = require("nativescript-angular/http");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        bootstrap: [
            app_component_1.AppComponent
        ],
        imports: [
            app_routing_1.AppRoutingModule,
            http_1.NativeScriptHttpModule
        ],
        declarations: [
            app_component_1.AppComponent,
            components_1.LoadingComponent,
            components_1.ErrorGooglePLayComponent,
        ],
        providers: [
            services_1.SharedService,
            services_1.ApiService,
            services_1.ImageService,
            modal_dialog_1.ModalDialogService
        ],
        schemas: [
            core_1.NO_ERRORS_SCHEMA
        ]
    })
], AppModule);
exports.AppModule = AppModule;
